<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>
<tr class="heading">
    <td colspan="2">Настройки для инфоблока</td>
</tr>
<tr class="adm-detail-valign-top">
    <td width="40%">Шаблон META TITLE</td>
    <td width="60%"><? echo IBlockInheritedPropertyInput($iIblockId, 'IBLOCK_META_TITLE', $arIPropertyTemplates, 'S') ?></td>
</tr>
<tr class="adm-detail-valign-top">
    <td width="40%">Шаблон META KEYWORDS</td>
    <td width="60%"><? echo IBlockInheritedPropertyInput($iIblockId, 'IBLOCK_META_KEYWORDS', $arIPropertyTemplates, 'S') ?></td>
</tr>
<tr class="adm-detail-valign-top">
    <td width="40%">Шаблон META DESCRIPTION</td>
    <td width="60%"><? echo IBlockInheritedPropertyInput($iIblockId, 'IBLOCK_META_DESCRIPTION', $arIPropertyTemplates, 'S') ?></td>
</tr>
<tr class="adm-detail-valign-top">
    <td width="40%">Заголовок страницы</td>
    <td width="60%"><? echo IBlockInheritedPropertyInput($iIblockId, 'IBLOCK_PAGE_TITLE', $arIPropertyTemplates, 'S') ?></td>
</tr>