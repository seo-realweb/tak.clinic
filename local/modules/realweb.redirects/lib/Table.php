<?php


namespace Realweb\Redirects;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class RedirectsTable
 * @package Realweb\Redirects
 */
class Table extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'realweb_redirects';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('REDIRECTS_ENTITY_ID_FIELD'),
            ),
            'UF_ACTIVE' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REDIRECTS_ENTITY_UF_ACTIVE_FIELD'),
            ),
            'UF_FROM' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REDIRECTS_ENTITY_UF_FROM_FIELD'),
            ),
            'UF_TO' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('REDIRECTS_ENTITY_UF_TO_FIELD'),
            ),
            'UF_SORT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('REDIRECTS_ENTITY_UF_SORT_FIELD'),
            ),
        );
    }
}