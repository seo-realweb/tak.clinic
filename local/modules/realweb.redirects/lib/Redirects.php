<?php

namespace Realweb\Redirects;

use Bitrix\Main\Loader;
use Bitrix\Main\Context;
use Bitrix\Main\ORM\EntityError;
use Bitrix\Main\Web\Uri;

class Redirects
{
    /**
     *
     * @param \Bitrix\Main\ORM\Event $obEvent
     */
    public static function onBeforeSave($obEvent)
    {
        $arFields = $obEvent->getParameter('fields');
        $id = $obEvent->getParameter('id');
        $obUriFrom = new Uri($arFields['UF_FROM']);
        $obUriTo = new Uri($arFields['UF_TO']);
        $obResult = new \Bitrix\Main\ORM\EventResult();
        if (!$id) {
            $row = Table::query()
                ->setSelect(array('ID'))
                ->where('UF_FROM', '=', $obUriFrom->getPath())
                ->exec()
                ->fetch();
            if ($row) {
                $obResult->setErrors(array(new EntityError('Редирект уже существует')));
            }
        }
        $obResult->modifyFields(array(
            'UF_FROM' => $obUriFrom->getPath(),
            'UF_TO' => $obUriTo->getPath(),
        ));
        $obEvent->addResult($obResult);
    }

    public static function onPageStart()
    {
        if (!defined('ADMIN_SECTION') && Loader::includeModule('iblock') && Loader::includeModule('highloadblock')) {
            self::checkRedirects();
        }
    }

    private static function checkRedirects()
    {
        $obUri = new Uri(Context::getCurrent()->getRequest()->getRequestUri());
        if ($obUri->getPath()) {
            $arRedirect = Table::getRow(array(
                'filter' => array(
                    'UF_ACTIVE' => 1,
                    'UF_FROM' => $obUri->getPath(),
                ),
                'order' => array(
                    'UF_SORT' => 'ASC'
                ),
            ));

            if ($arRedirect) {
                if (strlen($arRedirect['UF_TO']) > 0 && $arRedirect['UF_FROM'] != $arRedirect['UF_TO']) {
                    LocalRedirect($arRedirect['UF_TO'], false, '301 Moved permanently');
                }
            }
        }
    }
}
