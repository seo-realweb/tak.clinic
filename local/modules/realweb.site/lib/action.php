<?php


namespace Realweb\Site;


use Bitrix\Main\Application;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Mail\Event;

class Action
{
    /**
     * @var HttpRequest
     */
    private $_request = null;

    private function _getRequest()
    {
        if ($this->_request === null) {
            $this->_request = Application::getInstance()->getContext()->getRequest();
        }

        return $this->_request;
    }

    public function confirmCookie()
    {
        Site::setCookie('confirm', 1, (time() + 60 * 60 * 24 * 30));

        return array('message' => '', 'result' => 1);
    }

    public function formSubmit()
    {
        $strName = $this->_getRequest()->get('name');
        $strEmail = $this->_getRequest()->get('email');
        $strPhone = $this->_getRequest()->get('phone');
        $strMessage = $this->_getRequest()->get('message');
        $strForm = $this->_getRequest()->get('form');

        $arForm = current(Iblock::getPropEnumValues(array('IBLOCK_ID' => IBLOCK_CONTENT_FORM_RESULT, 'CODE' => 'FORM', 'XML_ID' => $strForm)));

        $obElement = new \CIBlockElement();
        $iId = $obElement->Add(array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => IBLOCK_CONTENT_FORM_RESULT,
            'IBLOCK_SECTION_ID' => false,
            'NAME' => implode(" - ", array(
                $arForm['VALUE'],
                date('d.m.Y H:i:s'),
                $strName
            )),
            'PROPERTY_VALUES' => array(
                'NAME' => $strName,
                'PHONE' => $strPhone,
                'EMAIL' => $strEmail,
                'MESSAGE' => $strMessage,
                'FORM' => $arForm['ID']
            )
        ));
        if ($iId) {
            $arEventFields = array(
                "ID" => $iId,
                "NAME" => $strName,
                "EMAIL" => $strEmail,
                "PHONE" => $strPhone,
                "MESSAGE" => $strMessage,
                "LINK" => Site::getDomain() . '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=' . IBLOCK_CONTENT_FORM_RESULT . '&type=content&lang=ru&ID=' . $iId . '&find_section_section=-1&WF=Y',
                "DATETIME" => date('d.m.Y H:i:s'),
            );
            Event::send([
                "EVENT_NAME" => "FORM_FEEDBACK",
                "LID" => "s1",
                "C_FIELDS" => $arEventFields,
            ]);

            return array('result' => 1, 'message' => 'Ваша завяка успешно отправлена');
        } else {
            return array('result' => 0, 'message' => $obElement->LAST_ERROR);
        }
    }
}