<?php


namespace Realweb\Site\Geo;


use Bitrix\Main\Application;
use Realweb\Site\ArrayHelper;
use Realweb\Site\iblock;
use Realweb\Site\Site;

class City
{
    /**
     * @var array
     */
    private $_city = null;

    /**
     * @var bool
     */
    private $_use_geo = false;

    /**
     * @var City
     */
    protected static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getCityGeo()
    {
        $geo = new Base();
        $this->setUseGeo();

        return $geo->getCity();
    }

    public function setUseGeo($use = true)
    {
        $this->_use_geo = $use;

        return $this;
    }

    public function isUseGeo()
    {
        return $this->_use_geo;
    }

    private function _getCity($filter = array())
    {
        return Iblock::getIblockElement(array('filter' => $filter));
    }

    public static function init()
    {
        self::getInstance()->get();
    }

    public function get()
    {
        if ($this->_city === null) {
            if ($cityId = Application::getInstance()->getContext()->getRequest()->getCookie('city')) {
                $this->_city = $this->_getCity(array('ID' => $cityId));
            }
            if (!$this->_city) {
                if ($city = $this->getCityGeo()) {
                    $this->_city = $this->_getCity(array('NAME' => $city['city']['name_ru']));
                    if ($this->_city) {
                        $this->_city['GEO'] = $city;
                        Site::setCookie('city', $this->_city['ID'], (time() + 60 * 60 * 24 * 30));
                    }
                }
            }
            if (!$this->_city) {
                $this->_city = $this->_getCity(array('!PROPERTY_DEFAULT' => false));
            }
        }

        return $this->_city;
    }

    public function getPhone(bool $bFormatted = false)
    {
        if ($arCity = $this->get()) {
            if (!$bFormatted) {
                return preg_replace('/[^0-9+]/', '', ArrayHelper::getValue($arCity, 'PROPS.PHONE'));
            } else {
                return ArrayHelper::getValue($arCity, 'PROPS.PHONE');
            }
        }
    }

    public function getEmail()
    {
        if ($arCity = $this->get()) {
            return ArrayHelper::getValue($arCity, 'PROPS.EMAIL');
        }
    }

    public function getAddress()
    {
        if ($arCity = $this->get()) {
            return ArrayHelper::getValue($arCity, 'PROPS.ADDRESS');
        }
    }

    public function getWork()
    {
        if ($arCity = $this->get()) {
            return ArrayHelper::getValue($arCity, 'PROPS.WORK');
        }
    }

    public function getPicture()
    {
        if ($arCity = $this->get()) {
            return ArrayHelper::getValue($arCity, 'PREVIEW_PICTURE.SRC');
        }
    }

    public function getDepartment()
    {
        if ($arCity = $this->get()) {
            return ArrayHelper::getValue($arCity, 'PROPS.DEPARTMENT_ID');
        }
    }

    public function getLat()
    {
        if ($arCity = $this->get()) {
            $arCoords = explode(",", ArrayHelper::getValue($arCity, 'PROPS.COORDS'));

            return doubleval(ArrayHelper::getValue($arCoords, 0));
        }
    }

    public function getLng()
    {
        if ($arCity = $this->get()) {
            $arCoords = explode(",", ArrayHelper::getValue($arCity, 'PROPS.COORDS'));

            return doubleval(ArrayHelper::getValue($arCoords, 1));
        }
    }
}