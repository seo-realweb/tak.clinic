<?php


namespace Realweb\Site;


use Bitrix\Iblock\ElementTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;
use Bitrix\Main\Web\Uri;

class iblock
{
    const DEFAULT_CACHE_TIME = 3600;

    public static function getIBlockElements($arParams)
    {
        $arResult = [];
        $arParams['meta'] = ArrayHelper::getValue($arParams, 'meta', false);
        $arParams['nav'] = ArrayHelper::getValue($arParams, 'nav', false);
        $arParams['sections'] = ArrayHelper::getValue($arParams, 'sections', false);
        $arParams['filter'] = ArrayHelper::getValue($arParams, 'filter', array());
        if (Loader::includeModule("iblock") && $arParams['filter']) {
            global $CACHE_MANAGER;
            $cacheTime = ArrayHelper::getValue($arParams, 'cache_time', self::DEFAULT_CACHE_TIME);
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($arParams));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockElement::GetList(
                    ["SORT" => "ASC"],
                    $arParams['filter'],
                    false,
                    $arParams['nav'],
                    ["*", "PROPERTY_*"]
                );
                $arIblockIds = array();
                while ($obElement = $rsElement->GetNextElement()) {
                    $arFields = $obElement->GetFields();
                    if ($arFields['PREVIEW_PICTURE']) {
                        $arFields['PREVIEW_PICTURE'] = \CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    }
                    if ($arFields['DETAIL_PICTURE']) {
                        $arFields['DETAIL_PICTURE'] = \CFile::GetFileArray($arFields['DETAIL_PICTURE']);
                    }
                    $arFields['PROPERTIES'] = $obElement->GetProperties();
                    foreach ($arFields['PROPERTIES'] as &$prop) {
                        if ($prop['PROPERTY_TYPE'] == 'F') {
                            if (is_array($prop['VALUE'])) {
                                foreach ($prop['VALUE'] as $val) {
                                    $prop['DISPLAY_VALUE'][] = \CFile::GetFileArray($val);
                                }
                            } else {
                                $prop['DISPLAY_VALUE'] = \CFile::GetFileArray($prop['VALUE']);
                            }
                        }
                        $arFields['PROPS'][$prop['CODE']] = $prop['VALUE'];
                    }
                    if ($arParams['meta']) {
                        $ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues($arFields['IBLOCK_ID'], $arFields['ID']);
                        $arFields['META'] = $ipropValues->getValues();
                    }
                    if ($arParams['sections']) {
                        $arFields['SECTIONS'] = array();
                        $rsSections = \CIBlockElement::GetElementGroups($arFields['ID'], true);
                        while ($arSection = $rsSections->GetNext()) {
                            $arFields['SECTIONS'][] = $arSection;
                        }
                    }
                    $arResult[$arFields['ID']] = $arFields;
                    $arIblockIds[$arFields['IBLOCK_ID']] = $arFields['IBLOCK_ID'];
                }
                foreach ($arIblockIds as $iblockId) {
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . $iblockId);
                }
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }

        return $arResult;
    }

    public static function getIblockElement($arParams)
    {
        $arResult = self::getIBlockElements($arParams);
        if ($arResult) {
            return current($arResult);
        }
    }

    public static function getIblock(int $iId)
    {
        $arResult = array();
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = self::DEFAULT_CACHE_TIME;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . $iId);
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                if ($arResult = \CIBlock::GetByID($iId)->GetNext()) {
                    $obMetaClass = new \Bitrix\Iblock\InheritedProperty\IblockValues($iId);
                    $arResult['META'] = $obMetaClass->getValues();
                    $CACHE_MANAGER->RegisterTag("iblock_id_meta_" . $iId);
                    $CACHE_MANAGER->EndTagCache();
                    $cache->endDataCache($arResult);
                } else {
                    $cache->abortDataCache();
                }
            }
        }

        return $arResult;
    }

    public static function getTags(array $arParams = array())
    {
        $arItems = array();
        $arFilter = $arParams['FILTER'];
        $arFilter['ACTIVE'] = 'Y';
        $arFilter["ACTIVE_DATE"] = "Y";
        $strProperty = ArrayHelper::getValue($arParams, 'PROPERTY', 'TAGS');
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = self::DEFAULT_CACHE_TIME;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($arFilter) . "|" . $strProperty);
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arItems = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $arIblockIds = array();
                $rsElement = \CIBlockElement::GetList(array(), $arFilter, array("PROPERTY_" . $strProperty, "IBLOCK_ID"), false, array("IBLOCK_ID", "PROPERTY_" . $strProperty));
                while ($arElement = $rsElement->GetNext()) {
                    $arIblockIds[] = $arElement['IBLOCK_ID'];
                    $arItems[] = array('TAG' => $arElement["PROPERTY_" . $strProperty . '_VALUE'], 'CNT' => $arElement['CNT']);
                }
                foreach ($arIblockIds as $iblockId) {
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . $iblockId);
                }
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arItems);
            }
        }
        $obRequest = Application::getInstance()->getContext()->getRequest();
        $strBaseUrl = ArrayHelper::getValue($arParams, 'BASE_URL', Site::getCurrentPage());
        $iTotal = 0;
        foreach ($arItems as $key => &$arItem) {
            $iTotal += $arItem['CNT'];
            if (!$arItem['TAG']) {
                unset($arItems[$key]);
            } else {
                $obUrl = new Uri($strBaseUrl);
                $obUrl->addParams(array('tag' => $arItem['TAG']));
                $arItem['URL'] = $obUrl->getPathQuery();
                if ($obRequest->get('tag') == $arItem['TAG']) {
                    $arItem['SELECTED'] = true;
                } else {
                    $arItem['SELECTED'] = false;
                }
            }
        }
        array_unshift($arItems, array('TAG' => 'Все', 'CNT' => $iTotal, 'URL' => $strBaseUrl, 'SELECTED' => !$obRequest->get('tag')));
        if ($strTag = $obRequest->get('tag')) {
            if ($strFilterName = ArrayHelper::getValue($arParams, 'FILTER_NAME')) {
                if (empty($GLOBALS[$strFilterName])) {
                    $GLOBALS[$strFilterName] = array();
                }
                $GLOBALS[$strFilterName]["PROPERTY_" . $strProperty] = $strTag;
            }
        }

        return $arItems;
    }

    public static function getIblockSections($arParams)
    {
        $arResult = array();
        $arParams['meta'] = ArrayHelper::getValue($arParams, 'meta', false);
        $arParams['nav'] = ArrayHelper::getValue($arParams, 'nav', false);
        $arParams['filter'] = ArrayHelper::getValue($arParams, 'filter', array());
        $arParams['sort'] = ArrayHelper::getValue($arParams, 'sort', array("SORT" => "ASC"));
        $arParams['cnt_element'] = ArrayHelper::getValue($arParams, 'cnt_element', false);
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = ArrayHelper::getValue($arParams, 'cache_time', self::DEFAULT_CACHE_TIME);
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($arParams));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $arIblockIds = array();
                $rsElement = \CIBlockSection::GetList(
                    $arParams['sort'],
                    $arParams['filter'],
                    $arParams['cnt_element'],
                    array("*", "UF_*")
                );
                while ($arSection = $rsElement->GetNext()) {
                    $arResult[$arSection['ID']] = $arSection;
                    $arIblockIds[$arSection['IBLOCK_ID']] = $arSection['IBLOCK_ID'];
                }
                foreach ($arIblockIds as $iblockId) {
                    $CACHE_MANAGER->RegisterTag("iblock_id_" . $iblockId);
                }
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }

        return $arResult;
    }

    public static function getIblockSection($arParams)
    {
        $arResult = self::getIblockSections($arParams);
        if ($arResult) {
            return current($arResult);
        }
    }

    public static function getUserFieldEnumValues($arFilter, $arFilterValues = array())
    {
        $arResult = array();
        $cacheTime = 3600;
        $cacheDir = __FUNCTION__;
        $cacheId = md5(__FUNCTION__ . "|" . serialize(array($arFilter, $arFilterValues)));
        $cache = Cache::createInstance();
        if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
            $arResult = $cache->GetVars();
        } elseif ($cache->StartDataCache()) {
            $rows = UserFieldTable::getList(array(
                'select' => array('ID'),
                'filter' => $arFilter
            ))->fetch();
            $FIELD_STATUS_ID = $rows['ID'];
            $cUserFieldsEnum = new \CUserFieldEnum();
            $rows = $cUserFieldsEnum->GetList(array("ID" => "ASC"), array_merge(array('USER_FIELD_ID' => $FIELD_STATUS_ID), $arFilterValues));
            while ($row = $rows->Fetch()) {
                $arResult[$row['ID']] = $row;
            }
            $cache->endDataCache($arResult);
        }
        return $arResult;
    }

    public static function getProperties($iIblockId)
    {
        $arResult = [];
        $rsProperty = \CIBlockProperty::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array('IBLOCK_ID' => $iIblockId));
        while ($arItem = $rsProperty->GetNext()) {
            $arResult[$arItem["CODE"]] = $arItem;
        }

        return $arResult;
    }

    public static function getPropEnumValues($arFilter)
    {
        $arValues = [];
        $rsPropertyEnums = \CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), $arFilter);
        while ($enumFields = $rsPropertyEnums->GetNext()) {
            $arValues[$enumFields["ID"]] = $enumFields;
        }
        return $arValues;
    }

    public static function getPropValue($CODE, $IBLOCK_ID, $ELEMENT_ID)
    {
        $arResult = array();
        if (Loader::includeModule('iblock')) {
            $dbProperty = \CIBlockElement::getProperty(
                $IBLOCK_ID,
                $ELEMENT_ID,
                "sort",
                "asc",
                array("CODE" => $CODE)
            );
            while ($arProperty = $dbProperty->Fetch()) {
                $arResult[] = $arProperty;
            }
        }
        return $arResult;
    }

    public static function setPropValue($CODE, $VALUE, $IBLOCK_ID, $ELEMENT_ID)
    {
        if (Loader::includeModule('iblock')) {
            \CIBlockElement::SetPropertyValuesEx($ELEMENT_ID, $IBLOCK_ID, array($CODE => $VALUE ? $VALUE : false));
        }
    }

    public static function GetProductByIdOffer($ID)
    {
        if (\Bitrix\Main\Loader::includeModule("iblock")) {
            $res = \CIBlockElement::GetList(
                array(),
                array(
                    'ID' => $ID,
                    'ACTIVE' => 'Y',
                    'IBLOCK_ID' => IBLOCK_CATALOG_OFFERS
                ),
                false,
                array('nTopCount' => '1'),
                array('ID', 'IBLOCK_ID', 'PROPERTY_CML2_LINK')
            );
            if ($element = $res->GetNext()) {
                return $element['PROPERTY_CML2_LINK_VALUE'];
            }
        }
        return false;
    }

    public static function getProperty($CODE, $IBLOCK_ID, $ELEMENT_ID)
    {
        $arResult = array();
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $dbProperty = \CIBlockElement::getProperty(
                $IBLOCK_ID,
                $ELEMENT_ID,
                "sort",
                "asc",
                array("CODE" => $CODE)
            );
            while ($arProperty = $dbProperty->Fetch()) {
                $arResult[] = $arProperty;
            }
        }
        return $arResult;
    }
}