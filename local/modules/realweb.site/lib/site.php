<?php

namespace Realweb\Site;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Web\Uri;
use Realweb\BaseInclude\BaseIncludeTable;


class Site
{
    private static $_storage = array(
        'base_include' => array()
    );

    public static function getIncludeText($code)
    {
        if (empty(self::$_storage['base_include'][$code])) {
            if ($arItem = BaseIncludeTable::getByCode($code)->fetch()) {
                self::$_storage['base_include'][$code] = $arItem['TEXT'];
            }
        }

        return ArrayHelper::getValue(self::$_storage, array('base_include', $code), '');
    }

    public static function definders()
    {
        Loader::includeModule('iblock');
        $rsResult = \Bitrix\Iblock\IblockTable::getList(array(
            'select' => array('ID', 'IBLOCK_TYPE_ID', 'CODE'),
        ));
        while ($row = $rsResult->fetch()) {
            $CONSTANT = ToUpper(implode('_', array('IBLOCK', $row['IBLOCK_TYPE_ID'], $row['CODE'])));
            if (!defined($CONSTANT)) {
                define($CONSTANT, $row['ID']);
            }
        }
    }

    public static function localRedirect301($url)
    {
        LocalRedirect($url, false, "301 Moved Permanently");
        exit();
    }

    public static function isMainPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false) == SITE_DIR;
    }

    public static function isLocal()
    {
        return stripos(Application::getInstance()->getContext()->getRequest()->getHttpHost(), '.local') !== false;
    }

    public static function isShop()
    {
        return SITE_ID == 'sh';
    }

    public static function isCatalogPage()
    {
        return \CSite::InDir('/catalog/');
    }

    public static function is404Page()
    {
        return (defined("ERROR_404") && ERROR_404 == "Y");
    }

    public static function showIncludeText($code, $isHideChange = false)
    {
        global $APPLICATION;
        $APPLICATION->IncludeComponent(
            "realweb:base.include",
            ".default",
            array(
                "CODE" => $code,
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "SHOW_ICON" => $isHideChange ? "Y" : 'N',
            )
        );
    }

    public function showH1()
    {
        global $APPLICATION;
        $html = '';
        $H1 = $APPLICATION->GetTitle();
        if ($APPLICATION->GetPageProperty('HIDE_H1', $APPLICATION->GetProperty("HIDE_H1", "N")) != "Y") {
            $html = "<h1 class='title __large' id=\"pagetitle\">" . $H1 . "</h1>";
        }

        return $html;
    }

    public function getClassContent()
    {
        global $APPLICATION;

        return $APPLICATION->GetPageProperty('CONTENT_CLASS', "");
    }

    public static function setCookie($cookName, $value, $expire = false)
    {
        if ($cookName && $value) {
            $context = \Bitrix\Main\Application::getInstance()->getContext();
            $spread = (Option::get("main", "auth_multisite", "N") == "Y" ? (Cookie::SPREAD_SITES | Cookie::SPREAD_DOMAIN) : Cookie::SPREAD_DOMAIN);
            $cookie = new Cookie($cookName, $value, $expire ? $expire : (time() + 60 * 60 * 24));
            $cookie->setSpread($spread);
            $cookie->setHttpOnly(true);
            $context->getResponse()->addCookie($cookie);
        }
    }

    public static function getCookie($name, $default = null)
    {
        if (($value = Application::getInstance()->getContext()->getRequest()->getCookie($name)) !== null) {
            return $value;
        } else {
            return $default;
        }
    }

    public static function getMenu($iblockId, $level, $type = '', $arParams = array())
    {
        global $APPLICATION;
        $aMenuLinksExt = array();
        if (Loader::IncludeModule('iblock')) {
            $arFilter = array(
                "SITE_ID" => SITE_ID,
                "ID" => $iblockId
            );

            $dbIBlock = \CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
            $dbIBlock = new \CIBlockResult($dbIBlock);

            if ($arIBlock = $dbIBlock->GetNext()) {
                if (defined("BX_COMP_MANAGED_CACHE"))
                    $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_" . $arIBlock["ID"]);

                if ($arIBlock["ACTIVE"] == "Y") {
                    $filterSectionName = 'arrFilterSection' . $type;
                    $GLOBALS[$filterSectionName] = array();
                    $filterElementName = 'arrFilterElement' . $type;
                    $GLOBALS[$filterElementName] = array();
                    if ($type) {
                        $GLOBALS[$filterSectionName]['UF_' . strtoupper($type) . '_MENU'] = false;
                        $GLOBALS[$filterElementName]['PROPERTY_' . strtoupper($type) . '_MENU'] = false;
                    }
                    $aMenuLinksExt = $APPLICATION->IncludeComponent("realweb:menu.sections", "", array(
                        "IS_SEF" => "Y",
                        "SEF_BASE_URL" => "",
                        "SECTION_PAGE_URL" => $arIBlock['SECTION_PAGE_URL'],
                        "DETAIL_PAGE_URL" => $arIBlock['DETAIL_PAGE_URL'],
                        "IBLOCK_TYPE" => $arIBlock['IBLOCK_TYPE_ID'],
                        "IBLOCK_ID" => $arIBlock['ID'],
                        "DEPTH_LEVEL" => $level,
                        "CACHE_TYPE" => "A",
                        "SECTION_FILTER_NAME" => $filterSectionName,
                        "ELEMENT_FILTER_NAME" => $filterElementName,
                        'SECTION_SELECT' => ArrayHelper::getValue($arParams, 'section_select'),
                        'ELEMENTS_SELECT' => ArrayHelper::getValue($arParams, 'element_select'),
                        'SHOW_ELEMENTS' => 'Y'
                    ), false, array('HIDE_ICONS' => 'Y'));
                }
            }

            if (defined("BX_COMP_MANAGED_CACHE"))
                $GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
        }
        return $aMenuLinksExt;
    }

    public static function normalizePhone($phone)
    {
        return preg_replace('/[^0-9]/', "", $phone);
    }

    public static function getDomain()
    {
        return (\CMain::IsHTTPS() ? 'https' : 'http') . "://" . $_SERVER['SERVER_NAME'];
    }

    public static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    public static function getTreeMenu($arResult)
    {
        $arItems = [];
        $currentItem = false;
        $i = 0;
        $prev = 0;
        $parent = false;
        foreach ($arResult as $arItem) {
            if ($prev > $arItem['DEPTH_LEVEL']) {
                $currentItem = &$parent;
            }
            if ($arItem['DEPTH_LEVEL'] == 1) {
                $arItems[$i] = $arItem;
                $currentItem = &$arItems[$i];
            } else {
                $currentItem['CHILD'][$i] = $arItem;
                if ($arItem['IS_PARENT']) {
                    $parent = &$currentItem;
                    $currentItem = &$currentItem['CHILD'][$i];
                }
            }
            $prev = $arItem['DEPTH_LEVEL'];
            $i++;
        }

        return $arItems;
    }

    /**
     * @return Uri
     * @throws \Bitrix\Main\SystemException
     */
    public static function getUri()
    {
        $uri = new Uri(Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $uri->deleteParams(["bxajaxid"]);

        return $uri;
    }

    public function isExtendedPage()
    {
        return !isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') === false;
    }

    public static function isCheckGooglePageSpeed()
    {
        return strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') !== false;
    }

    public static function isMobile()
    {
        $obMobileDetect = new MobileDetect();

        return $obMobileDetect->isMobile() && !$obMobileDetect->isTablet();
    }

    public static function epilogAction()
    {
        $obSeo = Seo::getInstance();
        $obSeo->processRobots();
        $obSeo->processCanonical();
        $obSeo->processMeta();
        $obSeo->processBreadcrumbs();
        $obSeo->processOgMeta();
    }

    public static function getCurrentPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false);
    }

    public static function getResizePicture($image, int $iWidth, int $iHeight, $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL_ALT)
    {
        $thumb = \CFile::ResizeImageGet(is_array($image) ? $image['ID'] : $image, array('width' => $iWidth, 'height' => $iHeight), $resizeType);
        if ($thumb) {
            return $thumb['src'];
        }

        return is_array($image) ? $image['SRC'] : \CFile::GetPath($image);
    }

    public static function setStatusPage404()
    {
        \Bitrix\Iblock\Component\Tools::process404("", true, true, true, "");
    }

    public static function getClinic()
    {
        return Iblock::getIblockElement(array('filter' => array('IBLOCK_ID' => IBLOCK_CONTENT_CONTACTS, 'ACTIVE' => 'Y')));
    }

    public static function getClinicPhone()
    {
        return ArrayHelper::getValue(self::getClinic(), 'PROPS.PHONE');
    }

    public static function getClinicAddress()
    {
        return ArrayHelper::getValue(self::getClinic(), 'PROPS.ADDRESS');
    }

    public static function getClinicWorkTime()
    {
        return ArrayHelper::getValue(self::getClinic(), 'PROPS.WORKTIME');
    }
}
