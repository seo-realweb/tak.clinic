<?php

namespace Realweb\Site\Builder;

class HLBuilder extends \WS\ReduceMigrations\Builder\HighLoadBlockBuilder {

    public function getIblock($CODE) {
        $row = \Bitrix\Highloadblock\HighloadBlockTable::getRow(array(
                'filter' => array(
                    'NAME' => $CODE
                )
        ));
        return $row;
    }

}
