<?
namespace Realweb\Site\Property;

/**
 * Class \Realweb\Api\Model\Property\AutoCompleteString
 * 
 */

class AutoCompleteString {

    function GetUserTypeDescription() {
        return array(
            "PROPERTY_TYPE" => "S",
            "USER_TYPE" => "ElementAutoCompleteString",
            "DESCRIPTION" => 'Строка с автозаполнением',
            "GetPropertyFieldHtml" => Array(__CLASS__, "GetPropertyFieldHtml"),
            "GetPropertyFieldHtmlMulty" => Array(__CLASS__, "GetPropertyFieldHtmlMulty"),
            "GetAdminListViewHTML" => Array(__CLASS__, "GetAdminListViewHTML"),
        );
    }

    function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName) {
        if (strlen($value['VALUE']) > 0) {
            return $value['VALUE'];
        }
        return '';
    }

    function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName) {
        $VALUE = "";
        $arProperty['MD5'] = md5($arProperty['ID']);

        if ($strHTMLControlName['MODE'] == 'iblock_element_admin') {
            if (strlen($value["VALUE"]) > 0) {
                return $value["VALUE"];
            }
            return '';
        }

        // если значение установлено
        if ($value["VALUE"]) {
            $VALUE = $value["VALUE"];
        }
        if (intval($arProperty['ROW_COUNT']) > 1) {
            $html = '<textarea id="' . $strHTMLControlName['VALUE'] . '" onfocus="window.oObject[this.id] = new JsTc_' . $arProperty['MD5'] . '(this, []);" name="' . $strHTMLControlName['VALUE'] . '"  cols="' . $arProperty['COL_COUNT'] . '" rows="' . $arProperty['ROW_COUNT'] . '">' . $VALUE . '</textarea>';
        } else {
            $html = '<input autocomplete="off" id="' . $strHTMLControlName['VALUE'] . '" onfocus="window.oObject[this.id] = new JsTc(this, []);" name="' . $strHTMLControlName['VALUE'] . '" value="' . $VALUE . '" size="' . $arProperty['COL_COUNT'] . '" type="text">';
        }


        return $html . self::GetScript($arProperty);
    }

    function GetPropertyFieldHtmlMulty($arProperty, $value, $strHTMLControlName) {
        if ($strHTMLControlName['MODE'] == 'iblock_element_admin') {
            $arrValues = array();
            foreach ($value as $arValueKEY => $arValue) {
                if (strlen($arValue['VALUE']) > 0) {
                    $arrValues[] = $arValue['VALUE'];
                }
            }
            return implode(' / ', $arrValues);
        }

        $arProperty['MD5'] = md5($arProperty['ID']);
        $div_id = $arProperty['MD5'];
        $field_template = self::GetInputFieldTemplate($arProperty, $strHTMLControlName, "#VALUE#", "#KEY#");
        $field_template_blank = str_replace(array("#VALUE#", PHP_EOL), "", $field_template);
        $field_template_blank = str_replace('"', '\"', $field_template_blank);
        ob_start();
        ?>
        <table id="<?php echo $div_id; ?>">

            <?php foreach ($value as $arValueKEY => $arValue): ?>
                <?php
                $value_field = str_replace("#KEY#", $arValueKEY, $field_template);
                $value_field = str_replace("#VALUE#", $arValue['VALUE'], $value_field);
                echo $value_field;
                ?>
            <?php endforeach; ?>
            <?php $KEY = 0; ?>
            <?php
            $next_field = str_replace("#KEY#", 'n' . $KEY, $field_template);
            $next_field = str_replace(array("#VALUE#"), "", $next_field);
            ?>
            <?php echo $next_field; ?>
            <?php $KEY++; ?>
        </table>
        <input type="button" value="Добавить" 
               onclick="AddStringRow<?php echo $div_id; ?>()">
        <script>
            function AddStringRow<?php echo $div_id; ?>() {
                if (!window.String_<?php echo $div_id; ?>) {
                    window.String_<?php echo $div_id; ?> = {'KEY':<?php echo $KEY; ?>};
                }
                key = window.String_<?php echo $div_id; ?>.KEY;
                var tbl = document.getElementById('<?php echo $div_id; ?>');
                row_template = "<?php echo $field_template_blank; ?>";

                var re = /#KEY#/g;
                var sHTML = row_template.replace(re, 'n' + key);
                var cnt = tbl.rows.length;
                row_to_clone = -1;
                var oRow = tbl.insertRow(cnt + row_to_clone + 1);
                var oCell = oRow.insertCell(0);
                oCell.innerHTML = sHTML;

                window.String_<?php echo $div_id; ?>.KEY = parseInt(window.String_<?php echo $div_id; ?>.KEY) + 1;
            }
        </script>

        <?php
        $strResult = ob_get_contents();
        ob_end_clean();
        return $strResult . self::GetScript($arProperty);
    }

    public static function GetInputFieldTemplate($arProperty, $strHTMLControlName, $VALUE, $KEY = "#KEY#") {
        
        $strHTMLControlName['VALUE_ID'] = str_replace(array('[',']'), "_", $strHTMLControlName['VALUE']);
        
        ob_start();
        ?>
        <?php echo '<tr><td>'; ?>
        <?php if (intval($arProperty['ROW_COUNT']) > 1): ?>
            <textarea id="<?php echo $strHTMLControlName['VALUE_ID']; ?>_VALUE_<?php echo $KEY; ?>" onfocus="window.oObject[this.id] = new JsTc_<?php echo $arProperty['MD5']; ?>(this, []);" name="<?php echo $strHTMLControlName['VALUE']; ?>[<?php echo $KEY; ?>][VALUE]"  cols="<?php echo $arProperty['COL_COUNT']; ?>" rows="<?php echo $arProperty['ROW_COUNT']; ?>"><?php echo $VALUE; ?></textarea>
        <?php else: ?>
            <input autocomplete="off" type="text" onfocus="window.oObject[this.id] = new JsTc_<?php echo $arProperty['MD5']; ?>(this, []);" name="<?php echo $strHTMLControlName['VALUE']; ?>[<?php echo $KEY; ?>][VALUE]" id="<?php echo $strHTMLControlName['VALUE_ID']; ?>_VALUE_<?php echo $KEY; ?>" size="<?php echo $arProperty['COL_COUNT']; ?>" value="<?php echo $VALUE; ?>" />
        <?php endif; ?>
        <?php echo '</td></tr>'; ?>   
        <?php
        $strResult = ob_get_contents();
        ob_end_clean();

        return $strResult;
    }

    public static function GetScript($arProperty) {
        ob_start();
        $id = $arProperty['MD5'];
        ?>
        <script>
            function JsTc_<?php echo $id; ?>(oHandler, arSites, sParser)
            {
                var t = this;
                var tmp = 0;

                t.oObj = typeof oHandler == 'object' ? oHandler : false;
                t.arSites = arSites;
                // Arrays for data
                if (sParser)
                {
                    t.sExp = new RegExp("[" + sParser + "]+", "i");
                } else
                {
                    t.sExp = new RegExp(" ");
                }
                t.oLast = {"str": false, "arr": false};
                t.oThis = {"str": false, "arr": false};
                t.oEl = {"start": false, "end": false};
                t.oUnfinedWords = {};
                // Flags
                t.bReady = true;
                t.eFocus = true;
                // Array with results & it`s showing
                t.aDiv = null;
                t.oDiv = null;
                // Pointers
                t.oActive = null;
                t.oPointer = Array();
                t.oPointer_default = Array();
                t.oPointer_this = 'input_field';

                t.oObj.onblur = function ()
                {
                    t.eFocus = false;
                };
                t.oObj.onfocus = function () {
                    if (!t.eFocus)
                    {
                        t.eFocus = true;
                        setTimeout(
                                function ()
                                {
                                    t.CheckModif('focus');
                                }, 500);
                    }
                };

                t.CHttpRequest = new JCHttpRequest();

                t.oLast["arr"] = t.oObj.value.split(t.sExp);
                t.oLast["str"] = t.oLast["arr"].join(":");

                setTimeout(function () {
                    t.CheckModif('this')
                }, 500);

                this.CheckModif = function (__data)
                {
                    var
                            sThis = false, tmp = 0,
                            bUnfined = false, word = "",
                            cursor = {};

                    if (!t.eFocus)
                        return;

                    if (t.bReady && t.oObj.value.length > 0)
                    {
                        // Preparing input data
                        t.oThis["arr"] = t.oObj.value.split(t.sExp);
                        t.oThis["str"] = t.oThis["arr"].join(":");

                        // Getting modificated element
                        if (t.oThis["str"] && (t.oThis["str"] != t.oLast["str"]))
                        {
                            cursor['position'] = TCJsUtils.getCursorPosition(t.oObj);
                            if (cursor['position']['end'] > 0 && !t.sExp.test(t.oObj.value.substr(cursor['position']['end'] - 1, 1)))
                            {
                                cursor['arr'] = t.oObj.value.substr(0, cursor['position']['end']).split(t.sExp);
                                sThis = t.oThis["arr"][cursor['arr'].length - 1];

                                t.oEl['start'] = cursor['position']['end'] - cursor['arr'][cursor['arr'].length - 1].length;
                                t.oEl['end'] = t.oEl['start'] + sThis.length;
                                t.oEl['content'] = sThis;

                                t.oLast["arr"] = t.oThis["arr"];
                                t.oLast["str"] = t.oThis["str"];
                            }
                        }
                        if (sThis)
                        {
                            // Checking for UnfinedWords
                            for (tmp = 2; tmp <= sThis.length; tmp++)
                            {
                                word = sThis.substr(0, tmp);
                                if (t.oUnfinedWords[word] == '!fined')
                                {
                                    bUnfined = true;
                                    break;
                                }
                            }
                            if (!bUnfined)
                                t.Send(sThis);
                        }
                    }
                    setTimeout(function () {
                        t.CheckModif('this')
                    }, 500);
                };

                t.Send = function (sSearch)
                {
                    if (!sSearch)
                        return false;
                    var oError = {};

                    t.CHttpRequest.Action
                            = function (data)
                            {
                                var result = {};
                                t.bReady = true;
                                try
                                {
                                    eval("result = " + data + ";");
                                } catch (e)
                                {
                                    oError['result_unval'] = e;
                                }

                                if (TCJsUtils.empty(result))
                                    oError['result_empty'] = Errors['result_empty'];

                                try
                                {
                                    if (TCJsUtils.empty(oError) && (typeof result == 'object'))
                                    {
                                        if (!(result.length == 1 && result[0]['NAME'] == t.oEl['content']))
                                        {
                                            t.Show(result);
                                            return;
                                        }
                                    } else
                                    {
                                        t.oUnfinedWords[t.oEl['content']] = '!fined';
                                    }
                                } catch (e)
                                {
                                    oError['unknown_error'] = e;
                                }
                                return;
                            };
                    //alert(t.arSites);
                    var queryString = '/local/tools/search_autocomplete_string.php?IBLOCK_ID=<?php echo $arProperty['IBLOCK_ID']; ?>&PROPERTY_ID=<?php echo $arProperty['ID']; ?>&search=' + encodeURIComponent(sSearch);
                    try
                    {
                        if (t.arSites && t.arSites.constructor.toString().indexOf("Array") != -1)
                        {
                            for (var i = 0, length = t.arSites.length; i < length; i++)
                                queryString += '&site_id[]=' + encodeURIComponent(t.arSites[i]);
                        }
                        var ck_box = document.getElementById('ck_' + oHandler.id);
                        if (ck_box)
                        {
                            if (ck_box.checked)
                                queryString += '&order_by=NAME';
                        }
                    } catch (e) {
                    }
                    t.CHttpRequest.Send(queryString);
                };

                t.Show = function (result)
                {
                    t.Destroy();

                    var pos = BX.pos(t.oObj);

                    t.oDiv = document.body.appendChild(document.createElement("DIV"));
                    t.oDiv.id = t.oObj.id + '_div';
                    t.oDiv.className = "bx-popup-menu";
                    t.oDiv.style.position = 'absolute';
                    t.aDiv = t.Print(result, ['NAME', 'CNT']);
                    if (t.oDiv.offsetWidth < 300)
                        t.oDiv.style.width = t.oDiv.offsetWidth + "px";
                    else
                        t.oDiv.style.width = "300px";
                    t.oDiv.style.zIndex = 5000;

                    jsFloatDiv.Show(t.oDiv, pos["left"], pos["bottom"]);

                    BX.bind(document, "click", t.CheckMouse);
                    BX.bind(document, "keydown", t.CheckKeyword);
                };

                t.Print = function (aArr, aColumn)
                {
                    var aEl = null;
                    var sPrefix = '';
                    var sColumn = '';
                    var aResult = Array();
                    var aRes = Array();
                    var iCnt = 0;
                    var tmp = 0;

                    sPrefix = t.oDiv.id;
                    var str = '<table cellspacing="0" cellpadding="0" border="0"><tr><td class="popupmenu">' +
                            '<table cellspacing="0" cellpadding="0" border="0" width="100%">';
                    for (var i = 0, length = aArr.length; i < length; i++)
                    {
                        // Math
                        aEl = aArr[i];
                        aRes = Array();
                        aRes['ID'] = (aEl['ID'] && aEl['ID'].length > 0) ? aEl['ID'] : iCnt++;
                        aRes['GID'] = sPrefix + '_' + aRes['ID'];
                        aRes['NAME'] = TCJsUtils.htmlspecialcharsEx(aEl['NAME']);
                        aRes['CNT'] = aEl['CNT'];
                        aResult[aRes['GID']] = aRes;
                        t.oPointer.push(aRes['GID']);
                        // Graph
                        str += '<tr><td>' +
                                '<table cellspacing="0" cellpadding="0" border="0" class="popupitem" ' +
                                'onmouseout="window.oObject.' + t.oObj.id + '.Init(); this.className=\'popupitem\';" ' +
                                'onmouseover="window.oObject.' + t.oObj.id + '.Init(); this.className=\'popupitem popupitemover\'" ' +
                                'onclick="window.oObject.' + t.oObj.id + '.oActive=this.id;" ' +
                                'id="' + aRes['GID'] + '" name="' + sPrefix + '_table">' +
                                '<tr><td class="gutter"><div></div></td>' +
                                '<td class="item" id="' + aRes['GID'] + '_NAME" width="90%">' + aRes['NAME'] + '</td>' +
                                '<td class="item" id="' + aRes['GID'] + '_CNT" width="10%" align="right">' + aRes['CNT'] + '</td>' +
                                '</tr></table></td></tr>';
                    }
                    str += '</table></td></tr></table>';
                    t.oPointer.push('input_field');
                    t.oPointer_default = t.oPointer;
                    t.oDiv.innerHTML = str;
                    return aResult;
                };

                t.Destroy = function ()
                {
                    try
                    {
                        jsFloatDiv.Close(t.oDiv);
                        t.oDiv.parentNode.removeChild(t.oDiv);
                    } catch (e)
                    {
                    }

                    t.oPointer = Array();
                    t.oPointer_default = Array();
                    t.oPointer_this = 'input_field';
                    t.oDiv = null;
                    t.aDiv = null;
                    t.oActive = null;

                    BX.unbind(document, "click", t.CheckMouse);
                    BX.unbind(document, "keydown", t.CheckKeyword);
                };

                t.Replace = function ()
                {
                    if (typeof t.oActive == 'string')
                    {
                        var tmp = t.aDiv[t.oActive];
                        var tmp1 = '';
                        if (typeof tmp == 'object')
                        {
                            var elEntities = document.createElement("span");
                            elEntities.innerHTML = tmp['NAME'].replace(/&quot;/g, '"').replace(/&amp;/g, '&');
                            tmp1 = elEntities.innerHTML;
                        }
                        //this preserves leading spaces
                        var start = t.oEl['start'];
                        while (start < t.oObj.value.length && t.oObj.value.substring(start, start + 1) == " ")
                            start++;

                        t.oObj.value = t.oObj.value.substring(0, start) + tmp1.replace(/&lt;/g, '<').replace(/&gt;/g, '>') + t.oObj.value.substr(t.oEl['end']);
                        TCJsUtils.setCursorPosition(t.oObj, start + tmp1.length);
                    }
                    return;
                };

                t.Init = function ()
                {
                    t.oActive = false;
                    t.oPointer = t.oPointer_default;
                    t.Clear();
                    t.oPointer_this = 'input_pointer';
                };

                t.Clear = function ()
                {
                    var oEl = {}, ii = '';
                    oEl = t.oDiv.getElementsByTagName("table");
                    if (oEl.length > 0 && typeof oEl == 'object')
                    {
                        for (ii in oEl)
                        {
                            if (oEl.hasOwnProperty(ii))
                            {
                                var oE = oEl[ii];
                                if (oE.name == (t.oDiv.id + '_table') || (t.aDiv[oE.id]))
                                {
                                    oE.className = "popupitem";
                                }
                            }
                        }
                    }
                    return;
                };

                t.CheckMouse = function ()
                {
                    t.Replace();
                    t.Destroy();
                };

                t.CheckKeyword = function (e)
                {
                    if (!e)
                        e = window.event;
                    var
                            oP = null,
                            oEl = null,
                            ii = null;
                    if ((37 < e.keyCode && e.keyCode < 41) || (e.keyCode == 13))
                    {
                        t.Clear();

                        switch (e.keyCode)
                        {
                            case 38:
                                oP = t.oPointer.pop();
                                if (t.oPointer_this == oP)
                                {
                                    t.oPointer.unshift(oP);
                                    oP = t.oPointer.pop();
                                }

                                if (oP != 'input_field')
                                {
                                    t.oActive = oP;
                                    oEl = document.getElementById(oP);
                                    if (typeof oEl == 'object')
                                    {
                                        oEl.className = "popupitem popupitemover";
                                    }
                                }
                                t.oPointer.unshift(oP);
                                break;
                            case 40:
                                oP = t.oPointer.shift();
                                if (t.oPointer_this == oP)
                                {
                                    t.oPointer.push(oP);
                                    oP = t.oPointer.shift();
                                }
                                if (oP != 'input_field')
                                {
                                    t.oActive = oP;
                                    oEl = document.getElementById(oP);
                                    if (typeof oEl == 'object')
                                    {
                                        oEl.className = "popupitem popupitemover";
                                    }
                                }
                                t.oPointer.push(oP);
                                break;
                            case 39:
                                t.Replace();
                                t.Destroy();
                                break;
                            case 13:
                                t.Replace();
                                t.Destroy();
                                if (BX.browser.IsIE())
                                {
                                    e.returnValue = false;
                                    e.cancelBubble = true;
                                } else
                                {
                                    e.preventDefault();
                                    e.stopPropagation();
                                }
                                break;
                        }
                        t.oPointer_this = oP;
                    } else
                    {
                        t.Destroy();
                    }
                    return true;
                };
            }
        </script>
        <?php
        $strResult = ob_get_contents();
        ob_end_clean();

        return $strResult;
    }

}
