<?php


namespace Realweb\Site;


use Bitrix\Main\Application;
use Bitrix\Main\HttpRequest;
use Bitrix\Main\Page\Asset;

class Seo
{
    /**
     * @var HttpRequest
     */
    private $_request = null;

    /**
     * @var string
     */
    private $_canonical = null;

    /**
     * @var string
     */
    private $_meta_title = null;

    /**
     * @var string
     */
    private $_meta_description = null;

    /**
     * @var string
     */
    private $_meta_keywords = null;

    /**
     * @var string
     */
    private $_title = null;

    /**
     * @var array
     */
    private $_breadcrumbs = array();

    /**
     * @var bool
     */
    private $_no_index = false;

    private $_params = array();

    /**
     * @var Seo
     */
    protected static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getRequest()
    {
        if ($this->_request === null) {
            $this->_request = Application::getInstance()->getContext()->getRequest();
        }

        return $this->_request;
    }

    public function getCurPage()
    {
        global $APPLICATION;
        return $APPLICATION->GetCurPage(false);
    }

    public function processOgMeta()
    {
        global $APPLICATION;
        $APPLICATION->SetPageProperty('og:locale', 'ru_RU');
        $APPLICATION->SetPageProperty('og:type', 'website');
        $APPLICATION->SetPageProperty('og:title', $APPLICATION->GetPageProperty('title'));
        $APPLICATION->SetPageProperty('og:description', $APPLICATION->GetPageProperty('description'));
        $APPLICATION->SetPageProperty('og:url', Site::getDomain() . \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestUri());
        $APPLICATION->SetPageProperty('og:site_name', '');
        $APPLICATION->SetPageProperty('og:image', Site::getDomain() . ($APPLICATION->GetPageProperty('IMAGE') ?: (SITE_TEMPLATE_PATH . "/local/templates/main/images/logo.png")));
    }

    public function processRobots()
    {
        global $APPLICATION;
        if ($arRequestParams = $this->getRequest()->toArray()) {
            $arAllowParams = array('q', 'PAGEN_');
            $bIndex = false;
            foreach ($arRequestParams as $strKey => $value) {
                foreach ($arAllowParams as $strParam) {
                    if (stripos($strKey, $strParam) !== false) {
                        $bIndex = true;
                    }
                }
            }
            if (!$bIndex) {
                $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');
            }
        }
        if ($this->isNoIndex()) {
            $APPLICATION->SetPageProperty('robots', 'noindex, nofollow');
        }
    }

    public function processCanonical()
    {
        if ($this->getCanonical()) {
            Asset::getInstance()->addString('<link rel="canonical" href="' . Site::getDomain() . $this->getCanonical() . '"/>', true);
        } else {
            foreach ($this->getRequest()->toArray() as $key => $value) {
                if (stripos($key, 'PAGEN_') !== false) {
                    Asset::getInstance()->addString('<link rel="canonical" href="' . Site::getDomain() . $this->getCurPage() . '"/>', true);
                }
            }
        }
    }

    public function processMeta()
    {
        global $APPLICATION;
        if ($this->getMetaTitle()) {
            $APPLICATION->SetPageProperty('title', $this->getMetaTitle());
        }
        if ($this->getMetaDescription()) {
            $APPLICATION->SetPageProperty('description', $this->getMetaDescription());
        }
        if ($this->getMetaKeywords()) {
            $APPLICATION->SetPageProperty('keywords', $this->getMetaKeywords());
        }
        if ($this->getTitle()) {
            $APPLICATION->SetTitle($this->getTitle());
        }
    }

    public function processBreadcrumbs()
    {
        global $APPLICATION;
        if ($this->getBreadcrumbs()) {
            foreach ($this->getBreadcrumbs() as $arItem) {
                $APPLICATION->AddChainItem($arItem['name'], $arItem['url']);
            }
        }
    }

    /**
     * @return string
     */
    public function getCanonical(): ?string
    {
        return $this->_canonical;
    }

    /**
     * @param string $strCanonical
     * @return $this
     */
    public function setCanonical(string $strCanonical)
    {
        $this->_canonical = $strCanonical;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaTitle(): ?string
    {
        return $this->_meta_title;
    }

    /**
     * @param string $strMetaTitle
     * @return $this
     */
    public function setMetaTitle(string $strMetaTitle)
    {
        $this->_meta_title = $strMetaTitle;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): ?string
    {
        return $this->_meta_description;
    }

    /**
     * @param string $strMetaDescription
     * @return $this
     */
    public function setMetaDescription(string $strMetaDescription)
    {
        $this->_meta_description = $strMetaDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->_meta_keywords;
    }

    /**
     * @param string $strMetaKeywords
     * @return $this
     */
    public function setMetaKeywords(string $strMetaKeywords)
    {
        $this->_meta_keywords = $strMetaKeywords;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->_title;
    }

    /**
     * @param string $strTitle
     * @return $this
     */
    public function setTitle(string $strTitle)
    {
        $this->_title = $strTitle;
        return $this;
    }

    /**
     * @return array
     */
    public function getBreadcrumbs(): array
    {
        return $this->_breadcrumbs;
    }

    /**
     * @param array $arBreadcrumbs
     * @return $this
     */
    public function setBreadcrumbs(array $arBreadcrumbs)
    {
        $this->_breadcrumbs = $arBreadcrumbs;

        return $this;
    }

    public function addBreadcrumbs(string $strName, string $strUrl = '')
    {
        $this->_breadcrumbs[] = array(
            'name' => $strName,
            'url' => $strUrl
        );
    }

    public function isPagen()
    {
        foreach ($this->getRequest()->toArray() as $key => $value) {
            if (stripos($key, 'PAGEN_') !== false) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isNoIndex()
    {
        return $this->_no_index;
    }

    /**
     * @param bool $bNoIndex
     * @return $this
     */
    public function setNoIndex(bool $bNoIndex = true)
    {
        $this->_no_index = $bNoIndex;

        return $this;
    }

    public function setParam(string $strField, $value)
    {
        $this->_params[$strField] = $value;

        return $this;
    }

    public function getParam(string $strField)
    {
        return ArrayHelper::getValue($this->_params, $strField);
    }
}