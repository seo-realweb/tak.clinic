<?php


namespace Realweb\Site;


use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Bitrix\Main\UserTable;

/**
 * Class User
 * @package Realweb\Site
 * @method string getPersonalPhone()
 */
class User extends Data
{
    /**
     * @var \CUser
     */
    private $user;

    private $data;

    private $_groups = null;

    /**
     * @var User
     */
    protected static $instance;

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function getUser()
    {
        if (is_null($this->user)) {
            global $USER;
            if (!is_object($USER)) {
                $USER = new \CUser;
            }
            $this->user = $USER;
        }

        return $this->user;
    }

    protected function __construct()
    {
        $this->getData();
    }

    public function getData($iId = null)
    {
        if (is_null($this->data)) {
            if (is_null($iId)) {
                if ($this->isAuth()) {
                    $iId = $this->getUser()->GetID();
                }
            }
            $arFields = array_fill_keys(array_keys(UserTable::getMap()), '');
            $obSelect = UserTable::query()
                ->setSelect(['*', 'UF_*'])
                ->where("ID", "=", $iId);
            $rsResult = $obSelect->exec();
            if ($arUser = $rsResult->fetchRaw()) {
                $arFields = $arUser;
            }

            $this->data = $arFields;
        }

        return $this->data;
    }

    public function isAuth()
    {
        return $this->getUser()->IsAuthorized();
    }

    public function getId()
    {
        return $this->getUser()->GetID();
    }

    public function getGroups()
    {
        if ($this->_groups === null && $this->isAuth()) {
            $this->_groups = \Bitrix\Main\UserGroupTable::query()
                ->setSelect(array('*'))
                ->where('USER_ID', '=', $this->getId())
                ->registerRuntimeField(
                    'USER_GROUP',
                    new Reference(
                        'USER_GROUP',
                        \Bitrix\Main\GroupTable::class,
                        Join::on('this.GROUP_ID', 'ref.ID')
                    )
                )
                ->addSelect('USER_GROUP.*', 'USER_GROUP_')
                ->setOrder(array('USER_GROUP_C_SORT' => 'DESC'))
                ->exec()
                ->fetchAll();
        }

        return $this->_groups;
    }
}