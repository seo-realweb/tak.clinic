<?php

use Bitrix\Main\EventManager;
use Realweb\Site\Site;

EventManager::getInstance()->addEventHandler("main", "OnPageStart", "onPageStart");
EventManager::getInstance()->addEventHandler("iblock", "OnIBlockPropertyBuildList", Array("\Realweb\Site\Property\AutoCompleteString", "GetUserTypeDescription"));
function onPageStart()
{
    Site::definders();
}