<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if ($arResult): ?>
    <div class="index-about">
        <div class="container">
            <div class="row">
                <div class="col-5 d-none d-lg-block">
                    <img src="<?php echo $arResult['PREVIEW_PICTURE']['SRC']; ?>" alt="<?php echo $arResult['PREVIEW_PICTURE']['ALT']; ?>">
                </div>
                <div class="col-lg-7">
                    <div class="index-about-content">
                        <div>
                            <?php echo $arResult['PREVIEW_TEXT']; ?>
                        </div>
                        <div class="d-block d-lg-none mb-4 text-center"><img src="<?php echo $arResult['PREVIEW_PICTURE']['SRC']; ?>" alt="<?php echo $arResult['PREVIEW_PICTURE']['ALT']; ?>"></div>
                        <div>
                            <?php echo $arResult['DETAIL_TEXT']; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>