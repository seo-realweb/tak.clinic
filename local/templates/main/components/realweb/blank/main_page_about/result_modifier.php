<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */

$arResult = \Realweb\Site\iblock::getIblockElement(array(
    'filter' => array('IBLOCK_ID' => IBLOCK_CONTENT_BLOCK, 'CODE' => 'main_page_about', 'ACTIVE' => 'Y')
));