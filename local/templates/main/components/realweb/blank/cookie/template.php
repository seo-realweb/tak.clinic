<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if (!\Realweb\Site\Site::getCookie('confirm')): ?>
    <div class="cookie js-cookie">
        <div class="container">
            <div class="cookie-row">
                <div class="cookie-text">
                    <?php \Realweb\Site\Site::showIncludeText('COOKIE'); ?>
                </div>
                <div class="cookie-button">
                    <a href="#" class="btn btn-purple-i js-cookie-confirm">Согласиться</a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>