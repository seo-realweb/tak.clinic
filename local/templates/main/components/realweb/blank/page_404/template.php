<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="d-flex flex-column align-items-center">
    <div>
        <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/404.png">
    </div>
    <h1 class="title __h2 mt-5">Страница не найдена</h1>
    <a href="/" class="btn btn-purple __large mt-4 mt-md-5">На главную</a>
</div>