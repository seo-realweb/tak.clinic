<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="index-form">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-5 order-1 order-lg-0">
                <div class="form">
                    <span class="index-form-img-top"></span>
                    <span class="index-form-img-left"></span>
                    <span class="index-form-img-right"></span>
                    <form class="js-submit-form" method="post">
                        <input type="hidden" name="form" value="feedback">
                        <input type="hidden" name="action" value="Action_formSubmit">
                        <div class="form-group">
                            <label>Имя</label>
                            <input type="text" name="name" placeholder="Введите имя" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Номер телефона</label>
                            <input type="tel" name="phone" placeholder="+7 (___) ___-__-__" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Коментарий</label>
                            <textarea placeholder="Поле для ввода коментария" name="message" class="form-control" required></textarea>
                        </div>
                        <div class="form-group">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" checked required id="index-form-consent">
                                <label class="form-check-label" for="index-form-consent">
                                    Нажимая на кнопку, я принимаю <a href="/consent/">условия обработки персональных данных</a>
                                </label>
                            </div>
                        </div>
                        <div class="d-flex justify-content-md-end">
                            <button type="submit" class="btn btn-purple __large">Отправить</button>
                        </div>
                        <div class="js-response"></div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6 order-0 order-lg-1">
                <div class="title __line __h2 mt-lg-80 mt-xl-100">У вас остались вопросы?</div>
                <p class="mb-4">Задайте вопрос прямо сейчас. И наши специалисты<br> ответят на него максимально подробно.</p>

                <p>Также вы можете связаться с нами по телефону:<br/>
                    <a class="d-flex align-items-center mt-4 mt-md-0" href="tel:<?php echo \Realweb\Site\Site::getClinicPhone(); ?>"><span
                                class="icon __phone mr-2"></span> <?php echo \Realweb\Site\Site::getClinicPhone(); ?></a>
                </p>
            </div>
        </div>
    </div>
</div>