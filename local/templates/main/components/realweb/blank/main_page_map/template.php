<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if ($arResult = \Realweb\Site\Site::getClinic()): ?>
    <div class="index-map">
        <div class="index-map-google" id="map-contacts"></div>
        <div class="container">
            <div class="index-map-text">
                <?php echo $arResult['PROPS']['MAP_TEXT']['TEXT']; ?>
            </div>
        </div>
        <?php if ($arCoords = explode(",", $arResult['PROPS']['COORDS'])): ?>
            <script>
                realweb.map = {
                    title: "  <?php echo $arResult['PROPS']['ADDRESS']; ?>",
                    lat: <?php echo $arCoords[0]; ?>,
                    lng: <?php echo $arCoords[1]; ?>
                };
            </script>
        <?php endif; ?>
    </div>
<?php endif; ?>