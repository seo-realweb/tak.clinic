<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

if ($arIds = \Realweb\Site\ArrayHelper::getValue($arResult, 'PROPERTIES.PRICE.VALUE')) {
    $arResult['PRICE'] = \Realweb\Site\iblock::getIBlockElements(array('filter' => array('IBLOCK_ID' => IBLOCK_CONTENT_PRICE, 'ID' => $arIds, 'ACTIVE' => 'Y')));
}