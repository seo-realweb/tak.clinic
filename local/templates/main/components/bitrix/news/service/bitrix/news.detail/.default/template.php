<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$fSum = 0;
?>
<div class="text">
    <div class="row justify-content-between">
        <div class="col-12 d-md-none mb-4">
            <img class="w-100"
                 src="<?php echo $arResult['DETAIL_PICTURE']['SRC']; ?>"
                 alt="<?php echo $arResult['DETAIL_PICTURE']['ALT']; ?>"
                 title="<?php echo $arResult['DETAIL_PICTURE']['TITLE']; ?>">
        </div>
        <div class="col-12 col-md-7">
            <h1 class="title __h2"><?php echo $arResult['NAME']; ?></h1>
            <?php echo $arResult['DETAIL_TEXT']; ?>
        </div>
        <div class="col-md-5 col-lg-auto d-none d-md-block">
            <img src="<?php echo $arResult['DETAIL_PICTURE']['SRC']; ?>"
                 alt="<?php echo $arResult['DETAIL_PICTURE']['ALT']; ?>"
                 title="<?php echo $arResult['DETAIL_PICTURE']['TITLE']; ?>">
        </div>
    </div>
    <?php if ($arResult['PROPERTIES']['INDICATIONS']['VALUE'] || $arResult['PROPERTIES']['CONTRAINDICATIONS']['VALUE']): ?>
        <div class="box-white mt-5 d-none d-md-block">
            <div class="row">
                <?php if ($arResult['PROPERTIES']['INDICATIONS']['VALUE']): ?>
                    <div class="col-12 col-md-6">
                        <div class="title __h4">ПОКАЗАНИЯ</div>
                        <ul class="m-0">
                            <?php foreach ($arResult['PROPERTIES']['INDICATIONS']['VALUE'] as $strValue): ?>
                                <li><?php echo $strValue; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
                <?php if ($arResult['PROPERTIES']['CONTRAINDICATIONS']['VALUE']): ?>
                    <div class="col-12 col-md-6">
                        <div class="title __h4">ПРОТИВОПОКАЗАНИЯ</div>
                        <ul class="m-0">
                            <?php foreach ($arResult['PROPERTIES']['CONTRAINDICATIONS']['VALUE'] as $strValue): ?>
                                <li><?php echo $strValue; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="d-md-none">
            <?php if ($arResult['PROPERTIES']['INDICATIONS']['VALUE']): ?>
                <div class="box-white mt-2r">
                    <div class="title __h4">ПОКАЗАНИЯ</div>
                    <ul class="m-0">
                        <?php foreach ($arResult['PROPERTIES']['INDICATIONS']['VALUE'] as $strValue): ?>
                            <li><?php echo $strValue; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if ($arResult['PROPERTIES']['CONTRAINDICATIONS']['VALUE']): ?>
                <div class="box-white mt-2r">
                    <div class="title __h4">ПРОТИВОПОКАЗАНИЯ</div>
                    <ul class="m-0">
                        <?php foreach ($arResult['PROPERTIES']['CONTRAINDICATIONS']['VALUE'] as $strValue): ?>
                            <li><?php echo $strValue; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <?php if ($arResult['PRICE']): ?>
        <div class="box-price mt-2r">
            <?php foreach ($arResult['PRICE'] as $arItem): ?>
                <div class="box-price-item">
                    <div class="box-price-title"><?php echo $arItem['NAME']; ?></div>
                    <div class="box-price-value">
                        <?php echo number_format($arItem['PROPERTIES']['PRICE']['VALUE'], 0, '.', ' '); ?> ₽
                    </div>
                </div>
                <?php $fSum += $arItem['PROPERTIES']['PRICE']['VALUE']; ?>
            <?php endforeach; ?>
            <?php if (count($arResult['PRICE']) > 1 && $fSum): ?>
                <div class="box-price-item __total">
                    <div class="box-price-title">Итого:</div>
                    <div class="box-price-value">
                        <?php echo number_format($fSum, 0, '.', ' '); ?> ₽
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
</div>