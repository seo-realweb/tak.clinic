<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$arResult['SECTIONS'] = \Realweb\Site\iblock::getIblockSections(array('filter' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'SECTION_ID' => $arParams['PARENT_SECTION'])));
if (!$arResult['SECTIONS']) {
    $arResult['SECTIONS'] = $arResult['SECTION']['PATH'];
}
$arItems = array();
foreach ($arResult['ITEMS'] as $arItem) {
    $arItems[$arItem['IBLOCK_SECTION_ID']][] = $arItem;
}
$arResult['ITEMS'] = $arItems;