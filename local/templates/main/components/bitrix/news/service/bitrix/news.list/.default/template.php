<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="services">
    <? if ($arResult["ITEMS"]): ?>
        <? foreach ($arResult["SECTIONS"] as $arSection): ?>
            <?php if ($arItems = \Realweb\Site\ArrayHelper::getValue($arResult['ITEMS'], $arSection['ID'])): ?>
                <div class="services-title"><?php echo $arSection['NAME']; ?></div>
                <div class="row row-xs-8 mb-4">
                    <? foreach ($arItems as $arItem): ?>
                        <?php
                        if ($arItem['PROPERTIES']['CAPTION']['VALUE']) {
                            $strTitle = nl2br($arItem['PROPERTIES']['CAPTION']['VALUE']);
                        } else {
                            $strTitle = $arItem["NAME"];
                        }
                        if ($arItem['PROPERTIES']['SIZE']['VALUE_XML_ID']) {
                            $size = $arItem['PROPERTIES']['SIZE']['VALUE_XML_ID'];
                        } else {
                            $size = 'normal';
                        }
                        if ($size == 'large') {
                            $colClass = 'col-12 col-md-6 col-lg-4';
                            $linkClass = '__large';
                        } else {
                            $colClass = 'col-6 col-lg-4 col-xl-3';
                            $linkClass = '';
                        }
                        ?>
                        <div class="<?php echo $colClass; ?>">
                            <a href="<? echo $arItem["DETAIL_PAGE_URL"] ?>" class="services-item <?php echo $linkClass; ?>">
                                <img alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                     title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                                     src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
                                <div class="services-item-title"><? echo $strTitle ?></div>
                                <span class="services-item-link"><i class="fal fa-arrow-right"></i>Подробнее</span>
                            </a>
                        </div>
                    <? endforeach; ?>
                </div>
            <?php endif; ?>
        <? endforeach; ?>
    <?php endif; ?>
</div>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>
