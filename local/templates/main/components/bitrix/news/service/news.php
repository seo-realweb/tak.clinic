<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arIblock = \Realweb\Site\iblock::getIblock($arParams['IBLOCK_ID']);

$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "",
    array(
        "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" =>  $arParams['IBLOCK_TYPE'],
        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
        "SECTION_ID" => "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "N",
        "TOP_DEPTH" => "1",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => array('UF_LINK', 'UF_PREVIEW_TEXT'),
        "ADD_SECTIONS_CHAIN" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y",
    ),
    $component
);

if ($arIblock) {
    $APPLICATION->SetTitle($arIblock['META']['IBLOCK_PAGE_TITLE']);
    $APPLICATION->AddChainItem($arIblock['META']['IBLOCK_PAGE_TITLE']);
    $APPLICATION->SetPageProperty('title', $arIblock['META']['IBLOCK_META_TITLE']);
    $APPLICATION->SetPageProperty('description', $arIblock['META']['IBLOCK_META_DESCRIPTION']);
}