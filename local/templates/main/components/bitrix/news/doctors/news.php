<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arIblock = \Realweb\Site\iblock::getIblock($arParams['IBLOCK_ID']);

require "section.php";

if ($arIblock) {
    $APPLICATION->SetTitle($arIblock['META']['IBLOCK_PAGE_TITLE']);
    $APPLICATION->SetPageProperty('title', $arIblock['META']['IBLOCK_META_TITLE']);
    $APPLICATION->SetPageProperty('description', $arIblock['META']['IBLOCK_META_DESCRIPTION']);
}