<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
    <div class="col-12 col-lg-5 mb-5 mb-lg-0">
        <img src="<?php echo $arResult['DETAIL_PICTURE']['SRC']; ?>" alt="<?php echo $arResult['DETAIL_PICTURE']['ALT']; ?>"/>
    </div>
    <div class="col-12 col-lg-7">
        <h1 class="title __h2 m-0"><?php echo $arResult['NAME']; ?></h1>
        <div class="title __h5"><?php echo $arResult['PROPERTIES']['POST']['VALUE']; ?></div>

        <?php echo $arResult["DETAIL_TEXT"]; ?>
    </div>
</div>