<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="doctors-slider">
    <div class="row">
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <div class="col-6 col-md-4 col-xl-3">
                <a href="<?php echo $arItem['DETAIL_PAGE_URL']; ?>" class="doctors-slider-item">
                    <div class="doctors-slider-img">
                        <img src="<?php echo $arItem['PREVIEW_PICTURE']['SRC']; ?>"
                             alt="<?php echo $arItem['PREVIEW_PICTURE']['ALT']; ?>">
                    </div>
                    <div class="doctors-slider-title"><?php echo $arItem['NAME']; ?></div>
                    <div class="doctors-slider-text"><?php echo $arItem['PROPERTIES']['POST']['VALUE']; ?></div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>

