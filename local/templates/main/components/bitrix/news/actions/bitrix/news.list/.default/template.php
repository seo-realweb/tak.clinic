<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <div class="action">
        <div class="action-picture">
            <img alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                 title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"
                 src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>">
        </div>
        <div class="action-content">
            <div class="action-title"><? echo $arItem["NAME"] ?></div>
            <div class="action-text">
                <? echo $arItem["PREVIEW_TEXT"] ?>
            </div>
        </div>
    </div>
<? endforeach; ?>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>

