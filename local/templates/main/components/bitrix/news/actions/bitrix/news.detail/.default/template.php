<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="text">

    <?php if ($arResult['DETAIL_PICTURE'] && ArrayHelper::getValue($arResult, 'PROPERTIES.BANNER_TITLE.VALUE.TEXT')): ?>
        <div class="heading-banner" style="background-image: url('<?php echo $arResult['DETAIL_PICTURE']['SRC']; ?>')">
            <div class="d-none d-md-block">
                <h1 class="title __large __left __l-line">
                    <?php echo htmlspecialcharsback(ArrayHelper::getValue($arResult, 'PROPERTIES.BANNER_TITLE.VALUE.TEXT')); ?>
                </h1>
                <div class="heading-banner__text"> <?php echo htmlspecialcharsback(ArrayHelper::getValue($arResult, 'PROPERTIES.BANNER_TEXT.VALUE.TEXT')); ?></div>
            </div>
        </div>
    <?php endif; ?>

    <div class="row align-items-center mb-4">
        <div class="col-auto">
            <div class="text__date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></div>
        </div>
        <div class="col-auto">
            <div class="js-rating-<?php echo $arResult['ID']; ?>">
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
                <i class="fas fa-star"></i>
            </div>
        </div>
    </div>

    <h1 class="title __large __left __heading"><?= $arResult["NAME"] ?></h1>

    <? $APPLICATION->IncludeComponent(
        "realweb:blank",
        "iblock_tags",
        array(
            'FILTER' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y'),
            'TAGS' => $arResult['PROPERTIES']['TAGS']['VALUE'],
            'BASE_URL' => $arResult['LIST_PAGE_URL']
        )
    ); ?>

    <hr/>


    <? if ($arResult["NAV_RESULT"]): ?>
        <? if ($arParams["DISPLAY_TOP_PAGER"]): ?><?= $arResult["NAV_STRING"] ?><br/><? endif; ?>
        <? echo $arResult["NAV_TEXT"]; ?>
        <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?><br/><?= $arResult["NAV_STRING"] ?><? endif; ?>
    <? elseif ($arResult["DETAIL_TEXT"] <> ''): ?>
        <? echo $arResult["DETAIL_TEXT"]; ?>
    <? else: ?>
        <? echo $arResult["PREVIEW_TEXT"]; ?>
    <? endif ?>

</div>