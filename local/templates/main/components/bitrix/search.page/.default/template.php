<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<div class="row justify-content-center">
    <div class="col-12 col-xl-8">
        <h1 class="title __h2 text-center mt-3">Поиск на сайте</h1>
        <div class="search">
            <form class="form" name="search_form" id="search_form" action="" method="get">
                <div class="form-group">
                    <button class="search-submit" type="submit"><i class="fal fa-search"></i></button>
                    <input type="text" class="form-control" name="q" value="<?= $arResult["REQUEST"]["QUERY"] ?>"
                           placeholder="Поиск" required>
                    <a href="javascript: void(0);" onclick="document.search_form.q.value = '';" class="fal fa-times"></a>
                </div>
            </form>
            <?php if ($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false): ?>
                <div class="alert alert-info">Введите запрос для поиска</div>
            <?php elseif ($arResult["ERROR_CODE"] != 0): ?>
                <div class="alert alert-danger"><?php echo $arResult["ERROR_TEXT"]; ?></div>
            <?php elseif (count($arResult["SEARCH"]) > 0): ?>
                <div class="search-items">
                    <?php foreach ($arResult["SEARCH"] as $arItem): ?>
                        <div class="search-item">
                            <div class="title __h4"><? echo $arItem["TITLE_FORMATED"] ?></div>
                            <p><? echo $arItem["BODY_FORMATED"] ?></p>
                            <a href="<? echo $arItem["URL"] ?>" class="link"><? echo \Realweb\Site\Site::getDomain() . $arItem["URL"] ?></a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else: ?>
                <div class="alert alert-danger"><?php echo GetMessage("SEARCH_NOTHING_TO_FOUND"); ?></div>
            <?php endif; ?>
            <?php if ($arParams["DISPLAY_BOTTOM_PAGER"] != "N"): ?>
                <?php $arResult["NAV_STRING"] ?>
            <?php endif; ?>
        </div>
    </div>
</div>