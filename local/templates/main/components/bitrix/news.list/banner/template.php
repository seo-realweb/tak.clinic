<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="banner">
        <div class="container">
            <div class="owl-carousel owl-banner js-owl-banner">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                    <div class="banner-item">
                        <div class="row align-items-center">
                            <div class="col-12 d-block d-md-none">
                                <img class="banner-item-img" src="<?php echo $arItem['PREVIEW_PICTURE']['SRC']; ?>"
                                     alt="<?php echo $arItem['PREVIEW_PICTURE']['ALT']; ?>">
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="banner-item-title"><?php echo $arItem['PREVIEW_TEXT']; ?></div>
                                <div class="banner-item-text">
                                    <?php echo $arItem['DETAIL_TEXT']; ?>
                                </div>
                                <?php if ($arItem['PROPERTIES']['LINK']['VALUE']): ?>
                                    <div class="d-flex">
                                        <a href="<?php echo $arItem['PROPERTIES']['LINK']['DESCRIPTION']; ?>" class="btn btn-purple __large"><?php echo $arItem['PROPERTIES']['LINK']['VALUE']; ?></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-6 text-right d-none d-md-block">
                                <img class="banner-item-img" src="<?php echo $arItem['DETAIL_PICTURE']['SRC']; ?>"
                                     alt="<?php echo $arItem['DETAIL_PICTURE']['ALT']; ?>">
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>