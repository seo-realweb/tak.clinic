<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */

if ($arIblock = $arResult['IBLOCK']) {
    $APPLICATION->SetTitle($arIblock['META']['IBLOCK_PAGE_TITLE']);
    $APPLICATION->SetPageProperty('title', $arIblock['META']['IBLOCK_META_TITLE']);
    $APPLICATION->SetPageProperty('description', $arIblock['META']['IBLOCK_META_DESCRIPTION']);
}