<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

if ($arIblock = \Realweb\Site\iblock::getIblock($arParams['IBLOCK_ID'])) {
    $arResult['IBLOCK'] = $arIblock;
    $arResult['SECTIONS'] = \Realweb\Site\iblock::getIblockSections(array('filter' => array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y')));
    $arItems = array();
    foreach ($arResult['ITEMS'] as $arItem) {
        $arItems[$arItem['IBLOCK_SECTION_ID']][] = $arItem;
    }
    $arResult['ITEMS'] = $arItems;
    $this->getComponent()->setResultCacheKeys(array('IBLOCK'));
}