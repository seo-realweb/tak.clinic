<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1 class="title __h2"><?php echo $arResult['IBLOCK']['META']['IBLOCK_PAGE_TITLE']; ?></h1>
<?php echo $arResult['IBLOCK']['DESCRIPTION']; ?>

<? if ($arResult["ITEMS"]): ?>
    <? foreach ($arResult["SECTIONS"] as $arSection): ?>
        <?php if ($arItems = ArrayHelper::getValue($arResult['ITEMS'], $arSection['ID'])): ?>
            <div class="title __h4 mt-2r"><?php echo $arSection['NAME']; ?></div>
            <div class="box-price mt-3">
                <? foreach ($arItems as $arItem): ?>
                    <div class="box-price-item">
                        <div class="box-price-title"><?php echo $arItem['NAME']; ?></div>
                        <div class="box-price-value">
                            <?php echo number_format($arItem['PROPERTIES']['PRICE']['VALUE'], 0, '.', ' '); ?> ₽
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <? endif; ?>
    <?php endforeach; ?>
<? endif; ?>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>