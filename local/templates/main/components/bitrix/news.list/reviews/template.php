<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="index-reviews">
        <div class="container">
            <div class="title __line __h2 __quote">Отзывы от наших клиентов</div>
            <div class="owl-carousel reviews-slider owl-slider js-owl-reviews">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                    <div class="review">
                        <div class="review-img">
                            <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/reviews.png">
                        </div>
                        <div class="review-title"><?php echo $arItem['NAME']; ?></div>
                        <div class="review-text">
                            <?php echo $arItem['PREVIEW_TEXT']; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="show-all d-none d-md-flex"><a href="<?php echo str_replace('#SITE_DIR#/', SITE_DIR, $arResult['LIST_PAGE_URL']); ?>">Смотреть все отзывы</a></div>
        </div>
    </div>
<? endif; ?>