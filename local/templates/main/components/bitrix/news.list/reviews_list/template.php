<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<h1 class="title __h2 __quote __block"><?php echo $arResult['IBLOCK']['META']['IBLOCK_PAGE_TITLE']; ?></h1>
<?php echo $arResult['IBLOCK']['DESCRIPTION']; ?>
<? if ($arResult["ITEMS"]): ?>
    <div class="row">
        <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="review">
                    <div class="review-img">
                        <a href="#modal-reviews" class="js-modal-picture-link" data-toggle="modal" data-src="<?php echo $arItem['PREVIEW_PICTURE']['SRC']; ?>">
                            <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/reviews.png">
                        </a>
                    </div>
                    <div class="review-title"><?php echo $arItem['NAME']; ?></div>
                    <div class="review-text">
                        <?php echo $arItem['PREVIEW_TEXT']; ?>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<? endif; ?>

<? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
    <?= $arResult["NAV_STRING"] ?>
<? endif; ?>

<div class="modal modal-picture fade js-modal-picture" id="modal-reviews" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <img class="d-block w-100 js-modal-picture-img" alt="reviews">
            </div>
        </div>
    </div>
</div>