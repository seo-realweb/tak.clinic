<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

if ($arIblock = \Realweb\Site\iblock::getIblock($arParams['IBLOCK_ID'])) {
    $arResult['IBLOCK'] = $arIblock;
    $this->getComponent()->setResultCacheKeys(array('IBLOCK'));
}