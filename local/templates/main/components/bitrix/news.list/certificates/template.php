<? use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult["ITEMS"]): ?>
    <div class="index-cert">
        <div class="container">
            <div class="title __line __h2">Cертификаты и учредительные документы</div>

            <div class="owl-carousel cert-slider owl-slider js-owl-cert">
                <? foreach ($arResult["ITEMS"] as $key => $arItem): ?>
                    <a target="_blank" href="<?php echo $arItem['DISPLAY_PROPERTIES']['FILE']['FILE_VALUE']['SRC']; ?>" class="cert-slider-item">
                        <div class="cert-slider-img">
                            <img src="<?php echo $arItem['PREVIEW_PICTURE']['SRC']; ?>"
                                 alt="<?php echo $arItem['PREVIEW_PICTURE']['ALT']; ?>">
                        </div>
                        <div class="cert-slider-title"><?php echo $arItem['NAME']; ?></div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<? endif; ?>