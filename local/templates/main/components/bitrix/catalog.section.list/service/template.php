<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?php if ($arResult['SECTIONS']): ?>
        <div class="index-service">
            <div class="container">
                <div class="row">
                    <?php foreach ($arResult['SECTIONS'] as $arSection): ?>
                        <div class="col-12 col-lg-3">
                            <a href="<?php echo $arSection['UF_LINK'] ?: $arSection['SECTION_PAGE_URL']; ?>" class="index-service-item">
                            <span class="index-service-icon"><img src="<?php echo $arSection['PICTURE']['SRC']; ?>"
                                                                  alt="<?php echo $arSection['PICTURE']['ALT']; ?>"></span>
                                <span class="index-service-title"><?php echo $arSection['NAME']; ?></span>
                                <span class="index-service-text"><?php echo $arSection['UF_PREVIEW_TEXT']; ?></span>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
<?php endif; ?>