<?php use Realweb\Site\ArrayHelper;
use Realweb\Site\Site;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<?php if (!empty($arResult)): ?>
    <ul class="navbar-nav">
        <?php foreach ($arResult as $arItem): ?>
            <li class="nav-item">
                <a class="nav-link <?= $arItem['SELECTED'] && Site::getCurrentPage() == $arItem["LINK"] ? '__active' : ''; ?>"
                   href="<?= $arItem['SELECTED'] && Site::getCurrentPage() == $arItem["LINK"] ? 'javascript: void(0)' : ArrayHelper::getValue($arItem, 'PARAMS.UF.UF_LINK', $arItem["LINK"]); ?>">
                    <?= $arItem["TEXT"] ?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
<?php endif ?>