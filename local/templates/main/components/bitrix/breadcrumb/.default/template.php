<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

/**
 * @global CMain $APPLICATION
 */
global $APPLICATION;

//delayed function must return a string
if (empty($arResult))
    return "";

$class = "";

$strReturn = '';

$strReturn .= '<nav aria-label="breadcrumb"><ol class="breadcrumb">';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    $title = trim($title);
    $nextRef = ($index < $itemSize - 2 && $arResult[$index + 1]["LINK"] <> "" ? ' itemref="bx_breadcrumb_' . ($index + 1) . '"' : '');
    $child = ($index > 0 ? ' itemprop="child"' : '');
    $arrow = ($index > 0 ? '' : '');

    if ($arResult[$index]["LINK"] <> "" && $index != $itemSize - 1) {
        $strReturn .= '<li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb-item" id="bx_breadcrumb_' . $index . '"><a itemprop="item" href="' . $arResult[$index]["LINK"] . '"><span itemprop="name">' . $title . '</span></a><meta itemprop="position" content="' . ($index + 1) . '" /></li>';
    } else {
        $strReturn .= '<li itemprop="itemListElement" itemscope
          itemtype="https://schema.org/ListItem" class="breadcrumb-item active" aria-current="page"><span itemprop="name">' . $title . '</span><meta itemprop="position" content="' . ($index + 1) . '" /></li>';
    }
}

$strReturn .= '</ol></nav>';

return $strReturn;
?>