<?

use \Bitrix\Main\Page\Asset,
    \Realweb\Site\Site;
use Realweb\Site\Geo\City;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

CJSCore::Init(array("fx"));
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <?php if (!Site::isCheckGooglePageSpeed()): ?>
        <?php Site::showIncludeText("HEAD_BEFORE"); ?>
    <?php endif; ?>
    <title><? $APPLICATION->ShowTitle(); ?></title>
    <script data-skip-moving="true">
        var realweb = {
            template_path: '<?php echo SITE_TEMPLATE_PATH; ?>'
        };
        var meta = document.querySelector("meta[name='viewport']");
        var windowWidth = (window.innerWidth < window.screen.width) ? window.innerWidth : window.screen.width;
        if (windowWidth < 375) {
            meta.setAttribute("content", "width=" + 375 + ", user-scalable=no");
        }
    </script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat+Alternates:wght@400;600&display=swap" rel="stylesheet">
    <?php
    $APPLICATION->ShowHead();
    $APPLICATION->ShowMeta('og:locale');
    $APPLICATION->ShowMeta('og:type');
    $APPLICATION->ShowMeta('og:title');
    $APPLICATION->ShowMeta('og:description');
    $APPLICATION->ShowMeta('og:url');
    $APPLICATION->ShowMeta('og:site_name');
    $APPLICATION->ShowMeta('og:image');

    Asset::getInstance()->addString('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />');
    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/style.min.css');

    if (!Site::isCheckGooglePageSpeed()) {
        Asset::getInstance()->addString('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD7GlW8jOEe5GJs_LEdFSLHccqcSep8_zI&callback=initMap&libraries=&v=weekly" defer></script>');
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/script.min.js');
    }
    ?>
    <?php if (!Site::isCheckGooglePageSpeed()): ?>
        <?php Site::showIncludeText("HEAD_AFTER"); ?>
    <?php endif; ?>
</head>
<body class="<?php echo Site::isMainPage() ? '__main' : ''; ?>">

<?php if (!Site::isCheckGooglePageSpeed()): ?>
    <?php Site::showIncludeText("BODY_BEFORE"); ?>
<?php endif; ?>

<?php if ($USER->IsAdmin()): ?>
    <div id="panel">
        <?php $APPLICATION->ShowPanel(); ?>
    </div>
<?php endif; ?>

<header class="header" id="header">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">
                <a href="<?php echo Site::isMainPage() ? 'javascript: void(0)' : '/'; ?>" class="header-logo">
                    <img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/logo.png">
                </a>
            </div>
            <div class="col-auto d-none d-lg-block">
                <div class="header-col">
                    <span class="icon __address"></span>
                    <div>
                        <div class="header-col-title"><?php echo Site::getClinicAddress(); ?></div>
                        <div class="header-col-text"><?php echo Site::getClinicWorkTime(); ?></div>
                    </div>
                </div>
            </div>
            <div class="col-auto d-none d-md-block">
                <div class="header-col">
                    <span class="icon __phone"></span>
                    <div>
                        <div class="header-col-title"><a
                                    href="tel:<?php echo Site::getClinicPhone(); ?>"><?php echo Site::getClinicPhone(); ?></a>
                        </div>
                        <div class="header-col-text"><a href="#">Позвонить</a></div>
                    </div>
                </div>
            </div>
            <div class="col-auto d-none d-md-block">
                <div class="header-col">
                    <a href="/search/" class="icon __search"></a>
                    <a href="/personal/" class="icon __cabinet"></a>
                    <div>
                        <a class="btn btn-purple" href="#">Записаться</a>
                    </div>
                </div>
            </div>
            <div class="col-auto d-md-none">
                <div class="d-flex align-items-center">
                    <a href="/search/" class="icon __search"></a>
                    <button class="navbar-toggler"
                            type="button"
                            data-toggle="show"
                            data-target="#header"
                            aria-controls="navbar-top"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-md">
            <div class="collapse navbar-collapse" id="navbar-top">
                <div class="d-md-none">
                    <div class="header-links">
                        <a href="tel::<?php echo Site::getClinicPhone(); ?>"><span class="icon __phone"></span>Позвонить</a>
                        <div class="header-links-delimiter"></div>
                        <a href="/personal/"><span class="icon __cabinet"></span>Кабинет</a>
                    </div>
                    <div class="header-address">
                        <div class="header-address-title"><?php echo Site::getClinicAddress(); ?></div>
                        <div class="header-address-text"><?php echo Site::getClinicWorkTime(); ?></div>
                    </div>
                </div>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "top",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "CHILD_MENU_TYPE" => "",
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(""),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "A",
                        "MENU_CACHE_USE_GROUPS" => "N",
                        "ROOT_MENU_TYPE" => "top",
                        "USE_EXT" => "Y"
                    ),
                    false,
                    array(
                        'HIDE_ICONS' => 'Y'
                    )
                ); ?>
            </div>
        </nav>
    </div>
</header>

<?php if (!Site::isMainPage()): ?>
<div class="content <?php $APPLICATION->AddBufferContent(array('Realweb\Site\Site', 'getClassContent')); ?>">
    <div class="container">
        <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", ".default", array(
            "PATH" => "",
            "START_FROM" => "0",
            "COMPONENT_TEMPLATE" => ".default"
        ), false
        ); ?>
        <?php endif; ?>

