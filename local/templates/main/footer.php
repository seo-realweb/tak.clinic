<?

use Realweb\Site\Geo\City;
use \Realweb\Site\Site;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>

<?php if (!Site::isMainPage()): ?>
    <?php echo '</div></div>'; ?>
<?php endif; ?>

<footer class="footer">
    <div class="container">
        <div class="row justify-content-between align-items-center gap-md-60">
            <div class="col-auto">
                <a class="footer-logo" href="<?php echo Site::isMainPage() ? 'javascript: void(0)' : '/'; ?>"><img src="<?php echo SITE_TEMPLATE_PATH; ?>/images/logo-footer.svg"></a>
            </div>
            <div class="col d-none d-md-block"></div>
            <div class="col-auto d-none d-md-block">
                <a class="link-icon" href="tel:<?php echo Site::getClinicPhone(); ?>"><span class="icon __phone-gray"></span><?php echo Site::getClinicPhone(); ?></a>
            </div>
            <div class="col-auto d-none d-md-block">
                <div class="link-icon"><span class="icon __address-gray"></span><?php echo Site::getClinicAddress(); ?></div>
            </div>
            <div class="col-auto d-block d-md-none">
                <ul class="social">
                    <li><a href="#"><span class="icon __vk __purple"></span></a></li>
                    <li><a href="#"><span class="icon __instagram __purple"></span></a></li>
                </ul>
            </div>
        </div>
        <div class="row justify-content-between align-items-center mt-4">
            <div class="col-auto">
                <nav class="navbar navbar-expand">
                    <div class="collapse navbar-collapse">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "bottom",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(""),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_USE_GROUPS" => "N",
                                "ROOT_MENU_TYPE" => "bottom",
                                "USE_EXT" => "Y"
                            ),
                            false,
                            array(
                                'HIDE_ICONS' => 'Y'
                            )
                        ); ?>
                    </div>
                </nav>
            </div>
            <div class="col-auto d-none d-md-block">
                <div class="d-flex justify-content-end">
                    <?php Site::showIncludeText('SOCIAL'); ?>
                </div>
            </div>

            <div class="col-12 d-block d-md-none mt-4">
                <div class="footer-col">
                    <span class="icon __address"></span>
                    <div>
                        <div class="footer-col-title"><?php echo Site::getClinicAddress(); ?></div>
                        <div class="footer-col-text"><?php echo Site::getClinicWorkTime(); ?></div>
                    </div>
                </div>
            </div>
            <div class="col-12 d-block d-md-none mt-4">
                <div class="footer-col">
                    <span class="icon __phone"></span>
                    <div>
                        <div class="footer-col-title"><a href="tel:<?php echo Site::getClinicPhone(); ?>"><?php echo Site::getClinicPhone(); ?></a></div>
                        <div class="footer-col-text"><a href="#">Позвонить</a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>

<?php $APPLICATION->IncludeComponent("realweb:blank", "cookie", array()); ?>

<?php Site::epilogAction(); ?>
<?php Site::showIncludeText("BODY_AFTER"); ?>
</body>
</html>

