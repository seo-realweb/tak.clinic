$(document).ready(function () {
    $('.js-owl-banner').owlCarousel({
        loop: true,
        nav: true,
        items: 1,
        dots: true,
        margin: 0,
        navText: ['<i class="fal fa-long-arrow-left"></i>', '<i class="fal fa-long-arrow-right"></i>'],
        responsive: {
            0: {
                nav: false,
            },
            768: {
                nav: false,
            },
            1300: {
                nav: true,
            }
        }
    });
    $('.js-owl-doctors').owlCarousel({
        loop: true,
        nav: true,
        items: 4,
        dots: false,
        margin: 30,
        navText: ['', ''],
        responsive: {
            0: {
                items: 2,
                nav: false,
                margin: 15
            },
            768: {
                items: 2,
            },
            992: {
                items: 4,
                margin: 20,
            },
            1200: {
                items: 4
            }
        }
    });
    $('.js-owl-cert').owlCarousel({
        loop: true,
        nav: true,
        items: 5,
        dots: false,
        margin: 30,
        navText: ['', ''],
        responsive: {
            0: {
                items: 2,
                nav: false,
                margin: 15,
            },
            768: {
                items: 2,
            },
            992: {
                items: 5,
                margin: 20,
            },
            1200: {
                items: 5
            }
        }
    });

    $('.js-owl-reviews').owlCarousel({
        loop: true,
        nav: true,
        items: 3,
        dots: false,
        margin: 30,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 2,
                margin: 15,
            },
            992: {
                items: 3,
                margin: 15,
            },
            1200: {
                items: 3
            }
        }
    });

    $('[type="tel"], .js-phone').mask('+7 (000) 000-00-00');


    $(".js-gallery").lightGallery({
        thumbnail: true,
        selector: '.js-gallery-item'
    });

    $(document).on('click', '[data-toggle="show"]', function (e) {
        e.preventDefault();
        $($(this).attr('data-target')).toggleClass('__show');
        $('body').toggleClass('__fixed');
    });

    $(document).on('click', '.js-cookie-confirm', function (e) {
        e.preventDefault();
        $('.js-cookie').remove();
        var data = new FormData();
        data.append('action', 'Action_confirmCookie')
        submitData(data, $(this));
    });


});

$(document).on('click', '.js-modal-picture-link', function (e) {
    e.preventDefault();
    $('.js-modal-picture').find('.js-modal-picture-img').attr('src', $(this).attr('data-src'));
});

$(document).on('submit', '.js-submit-form', function (e) {
    e.preventDefault();
    var $form = $(this),
        data = new FormData($form[0]);
    submitData(data, $form);
    return false;
});

$(document).on('click', '.js-ajax-link', function (e) {
    e.preventDefault();
    var $this = $(this),
        json = JSON.parse($(this).attr('data-data')),
        data = new FormData();
    for (var i in json) {
        data.append(i, json[i]);
    }
    submitData(data, $this);

    return false;
});

function submitData(data, $this) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: "/ajax/",
        contentType: false,
        processData: false,
        data: data,
        beforeSend: function () {
            BX.closeWait();
            BX.showWait();
        }
    }).done(function (response) {
        if (response.message) {
            if (response.result === 1) {
                $this.find('.js-response').html('<div class="alert alert-success mt-4 mb-0">' + response.message + '</div>');
            } else {
                $this.find('.js-response').html('<div class="alert alert-danger mt-4 mb-0">' + response.message + '</div>');
            }
        }
        if (response.reload) {
            location.reload();
        }
        BX.closeWait();
    });
}

function initMap() {
    if (realweb.map) {
        var map = new google.maps.Map(document.getElementById("map-contacts"), {
            center: {lat: realweb.map.lat, lng: realweb.map.lng},
            zoom: 15,
        });
        var infoWindow = new google.maps.InfoWindow({
            content: 'Малый Казённый переулок, 12с2',
        });
        var marker = new google.maps.Marker({
            position: {lat: realweb.map.lat, lng: realweb.map.lng},
            map: map,
            title: 'Малый Казённый переулок, 12с2'
        });
        marker.addListener("click", function () {
            infoWindow.open(map, marker);
        });
    }
}