<?

define("STOP_STATISTICS", true);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
header('Content-Type: application/x-javascript; charset=' . LANG_CHARSET);
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");

if (CModule::IncludeModule("iblock")) {
    CUtil::decodeURIComponent($_REQUEST);
    if (!empty($_REQUEST["search"]) && intval($_REQUEST['IBLOCK_ID']) > 0 && intval($_REQUEST['PROPERTY_ID']) > 0) {
        $arResult = array();
        if ($_REQUEST["order_by"] == "NAME") {
            $arOrder = array('PROPERTY_' . $_REQUEST['PROPERTY_ID'] => "ASC");
        } else {
            $arOrder = array("CNT" => "DESC", 'PROPERTY_' . $_REQUEST['PROPERTY_ID'] => "ASC");
        }

        $arFilter = Array(
            "IBLOCK_ID" => $_REQUEST['IBLOCK_ID'],
            "!PROPERTY_" . $_REQUEST['PROPERTY_ID'] => false,
            "%PROPERTY_" . $_REQUEST['PROPERTY_ID'] => $_REQUEST["search"],
        );

        $res = CIBlockElement::GetList($arOrder, $arFilter, array('PROPERTY_' . $_REQUEST['PROPERTY_ID']), false);
        while ($theme = $res->GetNext()) {
            $arResult[] = array(
                "NAME" => $theme["PROPERTY_" . $_REQUEST['PROPERTY_ID'] . "_VALUE"],
                "CNT" => $theme["CNT"],
            );
        }
?><?= CUtil::PhpToJSObject($arResult) ?><?

    }
}
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin_js.php");
?>