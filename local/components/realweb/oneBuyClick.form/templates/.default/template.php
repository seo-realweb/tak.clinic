<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>

<div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <div class="h5 modal-title">Заказ в один клик</div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?php if ($arResult['MESSAGE']): ?>
                <div class="form-group form__response"><?= $arResult['MESSAGE'] ?></div>
                <div class="form-group text-center">
                    <button data-dismiss="modal" type="button" class="btn btn-default">OK</button>
                </div>
            <?php else: ?>
                <form action="<?= POST_FORM_ACTION_URI ?>" method="post">
                    <?= bitrix_sessid_post() ?>
                    <input type="hidden" name="action" value="one_buy_click">
                    <input type="hidden" name="shop" value="">
                    <input type="hidden" name="store_id" value="">
                    <input type="hidden" name="PRODUCT_ID" value="<?= $arParams['PRODUCT_ID'] ?>"/>
                    <input type="hidden" name="OFFER_ID" value=""/>

                    <div class="form-group">
                        <input type="text" name="NAME" maxlength="50" value="" class="form-control"
                               placeholder="Введите ФИО" required="">
                    </div>
                    <div class="form-group">
                        <input type="text" name="EMAIL" maxlength="50" value="" class="form-control"
                               placeholder="Email адрес" required="">
                    </div>
                    <div class="form-group">
                        <input type="tel" name="PERSONAL_PHONE" maxlength="50" value="" class="form-control"
                               placeholder="Введите телефон" required="">
                    </div>
                    <div class="form__response"><?= $arResult['MESSAGE'] ?></div>
                    <div class="form-group text-center">
                        <button type="submit" class="btn btn-default">Сделать заказ</button>
                    </div>
                </form>
            <?php endif; ?>
        </div>
    </div>
</div>