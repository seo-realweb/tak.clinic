<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Data\Cache;

class CRealwebOneByClickComponent extends CBitrixComponent
{
    protected $request = array();

    public function onPrepareComponentParams($params)
    {
        $this->request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
        if (!isset($params["CACHE_TIME"]))
            $params["CACHE_TIME"] = 3600;
        return $params;
    }

    private function LoadDataShop()
    {
        $arResult = [];
        $arResult['DELIVERY'] = \Bitrix\Sale\Delivery\Services\Manager::getActiveList();
        $res = \Bitrix\Sale\PaySystem\Manager::getList(array('select' => array('*'), 'order' => array('SORT' => 'ASC')));
        while ($row = $res->fetch()) {
            if ($row['ACTION_FILE'] != 'inner')
                $arResult['PAY_SYSTEM'][] = $row;
        }
        $res = \CSalePersonType::GetList(array("SORT" => "ASC"), array("LID" => SITE_ID));
        while ($row = $res->Fetch()) {
            $arResult['PERSON_TYPE'][] = $row;
        }
        $res = \CSaleStatus::GetList();
        while ($row = $res->Fetch()) {
            $arResult['STATUS'][] = $row;
        }
        $dbResult = \CCatalogStore::GetList(
            array(),
            array('ACTIVE' => 'Y'),
            false,
            false,
            array("*")
        );
        while ($row = $dbResult->Fetch()) {
            $arResult['STORE'][$row['ID']] = $row;
        }

        return $arResult;
    }

    public static function GetProps($CODE = false)
    {
        $arProps = array();
        if (Loader::includeModule("sale")) {
            $filter = array();
            if ($CODE) {
                $filter = array("CODE" => $CODE);
            }
            $arProps = \Bitrix\Sale\Internals\OrderPropsTable::getList(array(
                "select" => array("*"),
                "filter" => $filter,
                "order" => array("SORT"),
            ))->fetchAll();
            if ($arProps) {
                $arProps = array_column($arProps, null, "CODE");
            }
        }
        return $arProps;
    }

    public function GetElementsData($IDS)
    {
        $arResult = [];
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
            $arSelect = array("*");
            $arFilter = array("ID" => $IDS);
            $res = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
            if ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arFields['PROPS'] = $ob->GetProperties();
                $arResult[$arFields['ID']] = $arFields;
            }
        }

        return $arResult;
    }

    private function CreateUser($arFields)
    {
        $dbUser = \Bitrix\Main\UserTable::getList(array(
            'select' => array('ID'),
            'filter' => array('EMAIL' => $arFields['EMAIL'])
        ));
        if ($arUser = $dbUser->fetch()) {
            return $arUser['ID'];
        }
        global $DB;
        $user = new \CUser;
        $arFields['PASSWORD'] = randString(8);
        $arUsers['VALUES'] = array(
            "NAME" => $arFields['NAME'],
            "EMAIL" => $arFields['EMAIL'],
            "LOGIN" => $arFields['EMAIL'],
            "PERSONAL_PHONE" => $arFields['PERSONAL_PHONE'],
            "ACTIVE" => "Y",
            "PASSWORD" => $arFields['PASSWORD'],
            "CONFIRM_PASSWORD" => $arFields['PASSWORD']
        );
        $arUsers['VALUES']["CHECKWORD"] = md5(\CMain::GetServerUniqID() . uniqid());
        $arUsers['VALUES']["~CHECKWORD_TIME"] = $DB->CurrentTimeFunction();
        $arUsers['VALUES']["CONFIRM_CODE"] = randString(8);
        $arUsers['VALUES']["LID"] = SITE_ID;
        $arUsers['VALUES']["USER_IP"] = $_SERVER["REMOTE_ADDR"];
        $arUsers['VALUES']["USER_HOST"] = @gethostbyaddr($_SERVER["REMOTE_ADDR"]);
        $def_group = \COption::GetOptionString("main", "new_user_registration_def_group", "");
        if ($def_group != "") {
            $arUsers['VALUES']["GROUP_ID"] = explode(",", $def_group);
        }

        if ($user_id = $user->Add($arUsers['VALUES'])) {
//            $arEventFields = $arUsers['VALUES'];
//            \Bitrix\Main\Mail\Event::send(array(
//                "EVENT_NAME" => "NEW_USER",
//                "LID" => \Bitrix\Main\Context::getCurrent()->getSite(),
//                "C_FIELDS" => $arEventFields,
//            ));
            return $user_id;
        }
        return false;
    }

    public function CreateOrder($arFields)
    {
        global $USER, $DB;
        $productId = !empty($arFields['OFFER_ID']) ? $arFields['OFFER_ID'] : $arFields['PRODUCT_ID'];
        if (\Bitrix\Main\Loader::includeModule('sale') && \Bitrix\Main\Loader::includeModule('catalog') && $productId) {
            $PRODUCTS = $this->GetElementsData([$productId]);
            if (!empty($PRODUCTS[$productId])) {
                $arProduct = $PRODUCTS[$productId];
                if ($USER->IsAuthorized()) {
                    $userId = $USER->GetID();
                } else {
                    $userId = $this->CreateUser([
                        'NAME' => $arFields['NAME'],
                        'EMAIL' => $arFields['EMAIL'],
                        'PERSONAL_PHONE' => $arFields['PERSONAL_PHONE']
                    ]);
                }
                if (!$userId) {
                    return array('STATUS' => 'ERROR', 'MESSAGE' => 'Ошибка регистрации пользователя');
                }
                $arData = $this->LoadDataShop();
                $arOrderProps = self::GetProps();
                $siteId = \Bitrix\Main\Context::getCurrent()->getSite();
                $currencyCode = \Bitrix\Currency\CurrencyManager::getBaseCurrency();
                $order = \Bitrix\Sale\Order::create($siteId, $userId);
                $order->setPersonTypeId(current($arData['PERSON_TYPE'])['ID']);
                $order->setField('CURRENCY', $currencyCode);
                if (!empty($arFields['shop'])) {
                    $order->setField('USER_DESCRIPTION', $arFields['shop']);
                }
                $basket = \Bitrix\Sale\Basket::create($siteId);
                //$arOrderProps = $this->GetProps();
                $properties = [
                    'PRODUCT.XML_ID' => [
                        'NAME' => 'Product XML_ID',
                        'CODE' => 'PRODUCT.XML_ID',
                        'VALUE' => $arProduct['XML_ID'],
                        'SORT' => 100
                    ],
                    'CATALOG.XML_ID' => [
                        'NAME' => 'Catalog XML_ID',
                        'CODE' => 'CATALOG.XML_ID',
                        'VALUE' => $arProduct['IBLOCK_EXTERNAL_ID'],
                        'SORT' => 100
                    ]
                ];
                if (!empty($arProduct['PROPS']['CML2_SIZE']['VALUE'])) {
                    $properties['CML2_SIZE'] = array(
                        'NAME' => $arProduct['PROPS']['CML2_SIZE']['NAME'],
                        'CODE' => 'CML2_SIZE',
                        'VALUE' => $arProduct['PROPS']['CML2_SIZE']['VALUE'],
                        'SORT' => 100
                    );
                }
                $item = $basket->createItem('catalog', $productId);
                $item->setFields(array(
                    'QUANTITY' => 1,
                    'CURRENCY' => $currencyCode,
                    'LID' => $siteId,
                    'PRODUCT_PROVIDER_CLASS' => '\Bitrix\Catalog\Product\CatalogProvider',
                    'PRODUCT_XML_ID' => $arProduct['XML_ID'],
                    'CATALOG_XML_ID' => $arProduct['IBLOCK_EXTERNAL_ID'],
                ));
                $item->save();
                $basketPropertyCollection = $item->getPropertyCollection();
                $basketPropertyCollection->setProperty($properties);
                $basketPropertyCollection->save();
                $order->setBasket($basket);
                $shipmentCollection = $order->getShipmentCollection();
                $shipment = $shipmentCollection->createItem();
                $shipment->setFields(array(
                    'DELIVERY_ID' => current($arData['DELIVERY'])['ID'],
                    'DELIVERY_NAME' => current($arData['DELIVERY'])['NAME'],
                ));

                $paySystem = current($arData['PAY_SYSTEM']);

                $paymentCollection = $order->getPaymentCollection();
                $payment = $paymentCollection->createItem();
                $payment->setFields(array(
                    'PAY_SYSTEM_ID' => $paySystem['ID'],
                    'PAY_SYSTEM_NAME' => $paySystem['NAME'],
                ));
                $propertyCollection = $order->getPropertyCollection();
                $phoneProp = $propertyCollection->getPhone();
                $phoneProp->setValue($arFields['PERSONAL_PHONE']);
                $nameProp = $propertyCollection->getPayerName();
                $nameProp->setValue($arFields['NAME']);
                $emailProp = $propertyCollection->getUserEmail();
                $emailProp->setValue($arFields['EMAIL']);

                // Устанавливаем тип покупки
                if (!empty($arOrderProps['FAST_ORDER'])) {
                    $sysTypePurchase = $propertyCollection->getItemByOrderPropertyId($arOrderProps['FAST_ORDER']['ID']);
                    $sysTypePurchase->setValue('Y');
                }
                \Bitrix\Sale\Notify::setNotifyDisable(true);
                $order->doFinalAction(true);
                $result = $order->save();
                //\Bitrix\Main\Diag\Debug::writeToFile($result->isSuccess());
                if ($result->isSuccess()) {
                    $_SESSION['SALE_ORDER_ID'][] = $order->getId();
                    $orderId = $order->getField('ACCOUNT_NUMBER');
                    if (!empty($arFields['store_id'])) {
                        if ($arStore = $arData['STORE'][$arFields['store_id']]) {
                            if ($arStore['EMAIL']) {
                                $strStoreEmail = $arStore['EMAIL'];
                            }
                        }
                    }
                    if (!empty($arFields['shop'])) {
                        $strEvent = 'SALE_NEW_SHOP_ORDER';
                    } else {
                        $strEvent = 'SALE_NEW_FAST_ORDER';
                    }
                    $event = new \CEvent;
                    $event->Send($strEvent, "s1", array(
                        "ORDER_ID" => $order->getId(),
                        "ORDER_DATE" => Date($DB->DateFormatToPHP(\CLang::GetDateFormat("SHORT", $siteId))),
                        "ORDER_USER" => $arFields['NAME'],
                        "PRICE" => \SaleFormatCurrency($order->getPrice(), $order->getCurrency()),
                        "BCC" => !empty($strStoreEmail) ? $strStoreEmail : \COption::GetOptionString("sale", "order_email", "order@" . $_SERVER['SERVER_NAME']),
                        "EMAIL_TO" => array("PAYER_NAME" => $arFields['NAME'], "USER_EMAIL" => $arFields['EMAIL']),
                        "ORDER_LIST" => implode("<br>", $basket->getListOfFormatText()),
                        "SALE_EMAIL" => \COption::GetOptionString("sale", "order_email", "order@" . $_SERVER['SERVER_NAME']),
                        "DELIVERY_PRICE" => 0,
                        "EMAIL" => $arFields['EMAIL'],
                        "COMMENT" => $order->getField('USER_DESCRIPTION')
                    ), "N");

                    return array('STATUS' => 'SUCCESS', 'ID' => $orderId);
                } else {
                    return array('STATUS' => 'ERROR', 'MESSAGE' => $result->getErrorMessages());
                }
            } else {
                return array('STATUS' => 'ERROR', 'MESSAGE' => 'Товар не найден');
            }
        }

        return false;
    }

    protected function doAction()
    {
        switch ($this->request['action']) {
            case 'one_buy_click':
                $result = $this->CreateOrder($this->request->toArray());
                if ($result['STATUS'] == 'SUCCESS')
                    $this->arResult['MESSAGE'] = 'Ваша заявка успешно принята. Менеджер свяжется с Вами в ближайшее время. № заказа ' . $result['ID'];
                else
                    $this->arResult['MESSAGE'] = $result['MESSAGE'];
                break;
        }
    }

    public function executeComponent()
    {
        if ($this->request['action']) {
            $this->doAction();
        }
        $this->IncludeComponentTemplate();
    }
}

?>