<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$this->setFrameMode(true);
?>
<div class="form __modal">
    <form action="<?= POST_FORM_ACTION_URI ?>" method="post">
        <?= bitrix_sessid_post() ?>
        <input type="hidden" name="action" value="saveRubrics">
        <input type="hidden" name="RUB_ID" id="RUB_ID" value="<?= $arParams['RUB_ID'] ?>"/>
        <div class="form-group">
            <input type="email" class="form-control" name="EMAIL" value="<?= $arResult["EMAIL"] ?>"
                   placeholder="Введите емайл" required>
        </div>
        <div class="form-group __submit">
            <button class="btn btn-orange __large" type="submit">Подписаться</button>
        </div>
        <?php if (!empty($arResult["MESSAGE"])): ?>
            <div class="form__response">
                <? if ($arResult["RESULT"] == 1): ?>
                    <div class="alert alert-success mb-0 mt-4">
                        <?= $arResult["MESSAGE"]; ?>
                    </div>
                <? else: ?>
                    <div class="alert alert-danger mb-0 mt-4">
                        <?= $arResult["MESSAGE"]; ?>
                    </div>
                <? endif; ?>
            </div>
        <?php endif; ?>
    </form>
</div>