<? use Bitrix\Main\Data\Cache;
use Bitrix\Main\Loader;
use Realweb\Site\ArrayHelper;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */

/** @global CMain $APPLICATION */
class CRealwebSheduleComponent extends CBitrixComponent
{

    public function onPrepareComponentParams($params)
    {
        return $params;
    }

    private function _getParam(string $code)
    {
        return $this->request->get($code);
    }

    private function _getSchedule()
    {
        $arSchedule = array(
            'items' => array(),
            'month' => array()
        );
        if (!empty($this->arParams['ID'])) {
            $arSection = \Realweb\Site\iblock::getIblockSection(array('filter' => array('ACTIVE' => 'Y', 'IBLOCK_ID' => \Realweb\Site\Site::getIblockContentId('schedule'), 'UF_IB_DOCTOR' => $this->arParams['ID'])));
            if ($arSection) {
                $arSections = \Realweb\Site\iblock::getIblockSections(array(
                    'filter' => array(
                        'ACTIVE' => 'Y',
                        'IBLOCK_ID' => \Realweb\Site\Site::getIblockContentId('schedule'),
                        'SECTION_ID' => $arSection['ID'],
                        '>=UF_D_DATE' => strtotime(date('Y-m-d 00:00:00'))
                    ),
                    'sort' => array('UF_D_DATE' => 'ASC')
                ));
                if ($arSections) {
                    $arElements = $this->_getElements(array_keys($arSections));
                    if ($arElements) {
                        foreach ($arSections as $arSection) {
                            $iMonth = FormatDate('m', $arSection['UF_D_DATE']);
                            if ($items = ArrayHelper::getValue($arElements, $arSection['ID'])) {
                                $arData = array(
                                    'time' => $arSection['UF_D_DATE'],
                                    'date' => FormatDate('d.m', $arSection['UF_D_DATE']),
                                    'week' => FormatDate('l', $arSection['UF_D_DATE']),
                                    'name' => $arSection['NAME'],
                                    'items' => $items
                                );
                                $arSchedule['items'][$iMonth][] = $arData;
                                $arSchedule['month'][$iMonth] = FormatDate('f', $arSection['UF_D_DATE']);
                            }

                        }
                    }
                }
            }
        }

        return $arSchedule;
    }

    private function _getElements($arSectionIds)
    {
        $arResult = array();
        if (Loader::includeModule("iblock") && $arSectionIds) {
            global $CACHE_MANAGER;
            $cacheTime = 3600;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($arSectionIds));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockElement::GetList(
                    array('PROPERTY_TM_TIME' => 'ASC'),
                    array(
                        'IBLOCK_ID' => \Realweb\Site\Site::getIblockContentId('schedule'),
                        'ACTIVE' => 'Y',
                        'SECTION_ID' => $arSectionIds
                    ),
                    false,
                    false,
                    array("ID", "IBLOCK_SECTION_ID", "IBLOCK_ID", "NAME", "PROPERTY_TM_TIME", "PROPERTY_B_FREE")
                );
                while ($arElement = $rsElement->GetNext()) {
                    if (time() + 3600 < $arElement['PROPERTY_TM_TIME_VALUE']) {
                        $arResult[$arElement['IBLOCK_SECTION_ID']][] = array(
                            'name' => $arElement['NAME'],
                            'id' => $arElement['ID'],
                            'time' => $arElement['PROPERTY_TM_TIME_VALUE'],
                            'free' => $arElement['PROPERTY_B_FREE_VALUE'],
                        );
                    }
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . \Realweb\Site\Site::getIblockContentId('schedule'));
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }

        return $arResult;
    }

    public function executeComponent()
    {
        $this->arResult = $this->_getSchedule();
        $this->arResult['current_month'] = key($this->arResult['month']);

        $this->includeComponentTemplate();
    }
}