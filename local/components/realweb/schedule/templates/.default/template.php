<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="block-padding __left __work">
    <div class="title __large text-md-left">Выбрать время для записи</div>

    <ul class="nav nav-links d-none d-md-flex" role="tablist">
        <?php foreach ($arResult['month'] as $key => $value): ?>
            <li class="nav-item" role="presentation">
                <a class="nav-link <?php echo $arResult['current_month'] == $key ? 'active' : ''; ?>" data-toggle="tab"
                   href="#month-<?php echo $key; ?>"><?php echo $value; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>

    <div class="owl-carousel js-tab js-links-slider __link __black link-group d-md-none">
        <?php foreach ($arResult['month'] as $key => $value): ?>
            <a class="js-tab-link <?php echo $arResult['current_month'] == $key ? '__active' : ''; ?>"
               href="#month-<?php echo $key; ?>"><?php echo $value; ?></a>
        <?php endforeach; ?>
    </div>

    <div class="tab-content">
        <?php foreach ($arResult['items'] as $month => $arItems): ?>
            <div data-parent="true" class="tab-pane <?php echo $arResult['current_month'] == $month ? 'active' : ''; ?>"
                 id="month-<?php echo $month; ?>">
                <div class="work">
                    <?php foreach ($arItems as $iKey => $arItem): ?>
                        <div class="work-item <?php echo $iKey > 7 ? "d-none" : ''; ?>" data-item="true"
                            <?php echo $iKey > 7 ? 'data-hide="d-none"' : ''; ?>>
                            <div class="work-box">
                                <div class="d-flex justify-content-between">
                                    <div class="work-date"><?php echo $arItem['date']; ?></div>
                                    <div class="work-week"><?php echo $arItem['week']; ?></div>
                                </div>
                                <div class="work-times">
                                    <?php foreach ($arItem['items'] as $item): ?>
                                        <?php if ($item['free'] == 'Y'): ?>
                                            <a href="#modal-schedule"
                                               data-toggle="modal"
                                               class="work-time js-modal-schedule"
                                               data-date="<?php echo $arItem['name']; ?>"
                                               data-time="<?php echo $item['name']; ?>"
                                               data-id="<?php echo $item['id']; ?>"><?php echo $item['name']; ?></a>
                                        <?php else: ?>
                                            <a href="javascript: void(0);"
                                               class="work-time __disabled"><?php echo $item['name']; ?></a>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <?php if (count($arItems) > 8): ?>
                    <div class="show-more __left" data-more="true">
                        <a href="#" class="btn btn-orange-o __sm __sm-cube __sm-large w-sm-100 js-show-more"
                           data-count="8">Смотреть еще</a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<div class="modal __large" tabindex="-1" role="dialog" id="modal-schedule">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title js-modal-title"></div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <form class="form __modal js-submit-form" action="/ajax/" method="post">
                            <input type="hidden" name="action" value="Action_scheduleMake">
                            <input type="hidden" name="schedule_id" class="js-schedule-id">
                            <input type="hidden" name="doctor_id" value="<?php echo $arParams['ID']; ?>">
                            <div class="form-group">
                                <label>
                                    <input type="text" name="name" class="form-control" placeholder="Ваше имя" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="tel" name="phone" class="form-control" placeholder="Телефон" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </label>
                            </div>
                            <div class="form-group">
                                <label>
                                    <textarea class="form-control" name="message" placeholder="Ваше сообщение"></textarea>
                                </label>
                            </div>
                            <div class="form-group __submit">
                                <div class="row">
                                    <div class="col-12 col-md-auto mb-3 mb-0">
                                        <button type="submit" class="btn btn-orange __large w-100 mw-100 w-md-auto">Подтвердить запись</button>
                                    </div>
                                    <div class="col-12 col-md-auto mb-3 mb-0">
                                        <button type="button" class="btn btn-orange-o __large w-100 w-md-auto" data-dismiss="modal">
                                            Выбрать другое время
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="js-response"></div>
                        </form>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="modal-text">
                            <p>Обратите внимание, что после записи на сайте вам обязательно перезвонит наш
                                администратор и подтвердит информацию.
                            </p>
                            <p>До момента подтверждения запись
                                недействительна.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>