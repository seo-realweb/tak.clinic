<?php
$arUrlRewrite = array(
    2 =>
        array(
            'CONDITION' => '#^/doctors/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/doctors/index.php',
            'SORT' => 100,
        ),
    1 =>
        array(
            'CONDITION' => '#^/actions/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/actions/index.php',
            'SORT' => 100,
        ),
        array(
            'CONDITION' => '#^/service/#',
            'RULE' => '',
            'ID' => 'bitrix:news',
            'PATH' => '/service/index.php',
            'SORT' => 100,
        ),
    0 =>
        array(
            'CONDITION' => '#^/rest/#',
            'RULE' => '',
            'ID' => NULL,
            'PATH' => '/bitrix/services/rest/index.php',
            'SORT' => 100,
        ),
);
