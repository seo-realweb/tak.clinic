<?
include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404", "Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
//$APPLICATION->IncludeComponent("realweb:redirect","",Array());
$APPLICATION->SetTitle("Такой страницы на сайте нет");
$APPLICATION->SetPageProperty('CONTENT_CLASS', "__center");
$APPLICATION->SetPageProperty('title', 'Страница не найдена');
?>
<?php $APPLICATION->IncludeComponent("realweb:blank", "page_404", array()); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>