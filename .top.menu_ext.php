<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Realweb\Site\Site;

$aMenuLinksExt = Site::getMenu(IBLOCK_CONTENT_SERVICE, 1, '', array('section_select' => array('UF_LINK')));
$aMenuLinks = array_merge($aMenuLinksExt, $aMenuLinks);